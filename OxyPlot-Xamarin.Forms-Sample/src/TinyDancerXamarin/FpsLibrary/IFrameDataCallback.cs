﻿namespace TinyDancerXamarin.FpsLibrary
{
    /**
    * Created by brianplummer on 11/12/15.
    */
    public interface IFrameDataCallback
    {
        /// <summary>
        /// this is called for every DoFrame() on the choreographer callback
        /// use this very judiciously.  Logging synchronously from here is a bad
        /// idea as doFrame will be called every 16-32ms.
        /// </summary>
        /// <param name="previousFrameNS">previous vsync frame time in NS</param>
        /// <param name="currentFrameNS">current vsync frame time in NS</param>
        /// <param name="droppedFrames">number of dropped frames between current and previous times</param>
        void DoFrame(long previousFrameNS, long currentFrameNS, int droppedFrames);
    }
}
