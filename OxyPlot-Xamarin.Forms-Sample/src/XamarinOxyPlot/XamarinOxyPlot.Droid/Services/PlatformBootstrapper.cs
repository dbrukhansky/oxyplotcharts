﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Practices.Unity;
using XamarinOxyPlot.Services;
using XamarinOxyPlot.Data;
using XamarinOxyPlot.Droid.Data;

namespace XamarinOxyPlot.Droid.Services
{
    public class PlatformBootstrapper : Bootstrapper
    {
        public override void Initialize(UnityContainer container)
        {
            base.Initialize(container);
            container.RegisterType<ILocalResourcesProvider, LocalResourcesProvider>();
        }
    }
}