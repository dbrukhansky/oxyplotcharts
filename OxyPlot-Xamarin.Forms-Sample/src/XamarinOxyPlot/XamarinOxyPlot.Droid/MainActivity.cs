﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using XamarinOxyPlot.Droid.Services;

namespace XamarinOxyPlot.Droid
{
    [Activity(Label = "XamarinOxyPlot", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.SensorLandscape)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();
            PlatformBootstrapper bootstrapper = new PlatformBootstrapper();
            bootstrapper.Run();
            LoadApplication(new App(bootstrapper));
        }
    }
}

