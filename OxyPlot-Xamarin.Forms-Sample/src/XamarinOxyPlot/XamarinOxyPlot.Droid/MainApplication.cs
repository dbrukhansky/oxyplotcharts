﻿using System;
using Android.App;
using Android.Runtime;

namespace XamarinOxyPlot.Droid
{
    [Application]
    public class MainApplication : Application
    {
        public MainApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            TinyDancerXamarin.FpsLibrary.TinyDancer.Create().Show(Context);
        }
    }
}
