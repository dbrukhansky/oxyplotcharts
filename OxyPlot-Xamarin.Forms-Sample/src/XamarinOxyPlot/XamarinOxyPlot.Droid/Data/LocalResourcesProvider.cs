﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using System.IO;
using System.Threading.Tasks;
using XamarinOxyPlot.Data;

namespace XamarinOxyPlot.Droid.Data
{
    public class LocalResourcesProvider : ILocalResourcesProvider
    {
        private AssetManager _assets; 

        public LocalResourcesProvider()
        {
            Context context = Application.Context.ApplicationContext;
            _assets = context.Resources.Assets;
        }

        public async Task<IEnumerable<string>> FindAsync(string path)
        {
            string[] files = await _assets.ListAsync(path);
            files = files.Select(fName => Path.Combine(path, fName)).ToArray();
            return files;
        }

        public Task<Stream> OpenAsync(string resourceName)
        {
            Stream stream = _assets.Open(resourceName);
            return Task.FromResult(stream);
        }
    }
}