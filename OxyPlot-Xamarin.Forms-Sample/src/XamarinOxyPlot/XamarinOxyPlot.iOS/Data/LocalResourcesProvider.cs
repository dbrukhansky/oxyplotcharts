﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Data;

namespace XamarinOxyPlot.iOS.Data
{
    public class LocalResourcesProvider : ILocalResourcesProvider
    {
        public Task<IEnumerable<string>> FindAsync(string path)
        {
            if (String.IsNullOrEmpty(path))
            {
                path = "/";
            }


            var files = Directory.GetFiles(path);
            return Task.FromResult<IEnumerable<string>>(files);
        }

        public Task<Stream> OpenAsync(string resourceName)
        {
            Stream stream = File.Open(resourceName, FileMode.Open, FileAccess.Read);
            return Task.FromResult(stream);
        }
    }
}
