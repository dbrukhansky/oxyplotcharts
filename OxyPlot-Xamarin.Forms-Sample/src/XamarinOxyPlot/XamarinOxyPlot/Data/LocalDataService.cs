﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Data
{
    public class LocalDataService
    {
        //private const string DateTimeFormat = "d.M.yyyy H:mm:ss";

        private static readonly string[] DateTimeFormats = new string[]
            {
                "d.M.yyyy H:mm:ss",
                "d.M.yyyy H:mm",
                "yyyy-M-d H:mm",
                "yyyy-M-d H:mm:ss",
            };

        private static ILocalResourcesProvider _resourcesProvider;

        public LocalDataService(ILocalResourcesProvider resourcesProvider)
        {
            _resourcesProvider = resourcesProvider;
        }

		public async Task<ChartData> LoadData()
        {
			return await Task.Run(async () =>
			{
				IEnumerable<string> resourceFiles = await _resourcesProvider.FindAsync("Data").ConfigureAwait(false);
				ChartData data = new ChartData();
				foreach (var file in resourceFiles)
				{

					using (Stream stream = await _resourcesProvider.OpenAsync(file))
					{
						string fileName = Path.GetFileName(file);

						//fileName != "Plot1.csv" &&
						if (
						   fileName == "Ranges.csv")

						    //
						{
						    continue;
						}

						DataLine line = ParseLine(fileName, stream);
						data.Lines.Add(line);
					}
				}

				GenerateTestData(data);

				return data;
			});
        }

        private void GenerateTestData(ChartData data)
        {
            foreach (var item in data.Lines)
            {
                if (item is PointsLine)
                {
                    PointsLine pointsLine = (PointsLine)item;
                    for (int i = 1; i < 10000; i++)
                    {
                        var copiedPoint = pointsLine.Points[i];
                        var previousPoint = pointsLine.Points[i - 1];
                        var difference = copiedPoint.DateTime - previousPoint.DateTime;
                        var lastPoint = pointsLine.Points.Last();
                        var newPoint = new TimelinePoint
                        {
                            DateTime = lastPoint.DateTime + difference,
                            Value = copiedPoint.Value
                        };

                        pointsLine.Points.Add(newPoint);
                    }
                }
            }

            for (int i = 0; i < 100; i++)
            {
                data.Lines.Add(data.Lines[i]);
            }

            CreateMixedDataArea(data);
            CreateFakeRangeItems(data);
        }

        private Random random = new Random();

        private void CreateMixedDataArea(ChartData data)
        {
            var lines = data.Lines.Where(l => l is PointsLine).Select(l => (PointsLine)l).ToArray();
            int index = 0;
            for (int i = 0; i < 20; i++)
            {
                RangesLine line = CreateFakeRangeItem(i);
                PointsLine points = lines[i];
                index++;
                data.Lines.Insert(index, line);
                index++;
                data.Lines.Insert(index, points);
            }
        }

        private RangesLine CreateFakeRangeItem(int nameIndex)
        {
            RangesLine line = new RangesLine
            {
                Name = "Fake range " + nameIndex,
            };

            DateTime currentDate = new DateTime(2017, 1, 1);
            for (int j = 0; j < 200; j++)
            {
                var hours = random.Next(4, 96);
                var endDate = currentDate.AddHours(hours);
                var range = new Range
                {
                    Text = "Range" + j,
                    Start = currentDate,
                    End = endDate,
                };

                line.Ranges.Add(range);
                currentDate = endDate.AddDays(1);
            }

            return line;
        }

        private void CreateFakeRangeItems(ChartData data)
        {
            for (int i = 0; i < 30; i++)
            {
                RangesLine line = CreateFakeRangeItem(i);
                data.Lines.Insert(i, line);
            }
        }

        protected DataLine ParseLine(string fileName, Stream stream)
        {
            DataLine line = null;

            using (StreamReader reader = new StreamReader(stream))
            {
                string result = reader.ReadToEnd();
                var lines = result.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 0)
                {
                    var firstLine = lines[0];
                    var rowParts = firstLine.Split(';');
                    switch (rowParts.Length)
                    {
                        case 2:
                            line = ParsePoints(lines);
                            break;

                        case 3:
                            line = ParseRanges(lines);
                            break;

                        default:
                            throw new InvalidOperationException("Incorrect data format");

                    }
                }
            }

            line.Name = Path.GetFileName(fileName);
            return line;
        }

        private DataLine ParsePoints(string[] lines)
        {
            List<TimelinePoint> points = new List<TimelinePoint>();
            foreach (var line in lines)
            {
                var parts = line.Split(';');
                if (parts.Length == 2)
                {
                    DateTime dateTime = new DateTime();
                    bool isSuccess = DateTime.TryParseExact(parts[0],
                                                           DateTimeFormats,
                                                           CultureInfo.InvariantCulture,
                                                           DateTimeStyles.None,
                                                           out dateTime);
                    if (!isSuccess)
                    {
                        ThrowLineParseException(line, "Date is incorrect");
                        continue;
                    }

                    double value = 0;
                    isSuccess = Double.TryParse(parts[1], out value);
                    if (!isSuccess)
                    {
                        ThrowLineParseException(line, "Value is incorrect");
                        continue;
                    }

                    points.Add(new TimelinePoint
                    {
                        DateTime = dateTime,
                        Value = value,
                    });
                }
                else
                {
                    ThrowLineParseException(line, "Can't split line correctly");
                    continue;
                }

            }

            return new PointsLine
            {
                Points = points.OrderBy(p => p.DateTime).ToList(),
            }; 
        }

        private DataLine ParseRanges(string[] lines)
        {
            List<Range> ranges = new List<Range>();
            foreach (var line in lines)
            {
                string[] parts = line.Split(';');
                if (parts.Length == 3)
                {
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();

                    bool isSuccess = DateTime.TryParseExact(parts[0], 
                                                            DateTimeFormats, 
                                                            CultureInfo.InvariantCulture, 
                                                            DateTimeStyles.None, 
                                                            out startDate);
                    if (!isSuccess)
                    {
                        ThrowLineParseException(line, "Start date is incorrect");
                        continue;
                    }

                    isSuccess = DateTime.TryParseExact(parts[1],
                                                        DateTimeFormats,
                                                        CultureInfo.InvariantCulture,
                                                        DateTimeStyles.None,
                                                        out endDate);

                    if (!isSuccess)
                    {
                        ThrowLineParseException(line, "End date is incorrect");
                        continue;
                    }


                    string name = parts[2];
                    ranges.Add(new Range
                    {
                        Start = startDate,
                        End = endDate,
                        Text = parts[2],
                    });
                }
                else
                {
                    ThrowLineParseException(line, "Parts count is incorrect");
                    continue;
                }

            }

            return new RangesLine
            {
                Ranges = ranges.OrderBy(r => r.Start).ToList(),
            };
        }

        private void ThrowLineParseException(string line, string message, Exception innerException = null)
        {
            string errorMessage = String.Format("Can't parse line \"{0}\": {1}", line, message);
            throw new InvalidOperationException(errorMessage, innerException);
        }
    }
}
