﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Data
{
    public class TimelinePoint
    {
        public DateTime DateTime { get; set; }

        public double Value { get; set; }
    }
}
