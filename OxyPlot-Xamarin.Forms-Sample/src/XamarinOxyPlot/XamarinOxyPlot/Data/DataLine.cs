﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Data
{
    public class ChartData
    {
        public ChartData()
        {
            Lines = new List<DataLine>();
        }

        public List<DataLine> Lines { get; set; }
    }


    public class DataLine
    {
        public string Name { get; set; }
    }

    public class RangesLine : DataLine
    {
        public RangesLine()
        {
            Ranges = new List<Range>();
        }

        public List<Range> Ranges { get; set; }
    }

    public class PointsLine : DataLine
    {
        public PointsLine()
        {
            Points = new List<TimelinePoint>();
        }

        public List<TimelinePoint> Points { get; set; }
    }
}
