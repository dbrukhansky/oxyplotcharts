﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Data
{
    public class Range
    {
        public string Text { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
