﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Data
{
    public interface ILocalResourcesProvider
    {
        Task<IEnumerable<string>> FindAsync(string path);

        Task<Stream> OpenAsync(string resourceName);
    }
}
