﻿using Microsoft.Practices.Unity;
using Xamarin.Forms;
using XamarinOxyPlot.Services;
using XamarinOxyPlot.ViewModels.Base;
using XamarinOxyPlot.Views;

namespace XamarinOxyPlot
{
    public partial class App : Application
    {
        private static ViewModelLocator _locator;

        public void Initialize()
        {
        }

        public static ViewModelLocator Locator
        {
            get { return _locator; }
        }

        public App(Bootstrapper bootstrapper)
        {
            _locator = bootstrapper.Container.Resolve<ViewModelLocator>();
            InitializeComponent();
            MainPage = new NavigationPage(new MainView());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
