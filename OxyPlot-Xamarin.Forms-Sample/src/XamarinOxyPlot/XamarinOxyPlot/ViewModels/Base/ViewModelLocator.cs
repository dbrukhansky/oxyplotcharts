﻿using Microsoft.Practices.Unity;
using XamarinOxyPlot.Services.Navigation;

namespace XamarinOxyPlot.ViewModels.Base
{
    public class ViewModelLocator
    {
        readonly IUnityContainer _container;

        public ViewModelLocator(UnityContainer container)
        {
            _container = container; 
        }

        public MainViewModel MainViewModel
        {
            get { return _container.Resolve<MainViewModel>(); }
        }

        public TimelineSampleViewModel TimelineSampleViewModel
        {
            get { return _container.Resolve<TimelineSampleViewModel>(); }
        }

    }
}
