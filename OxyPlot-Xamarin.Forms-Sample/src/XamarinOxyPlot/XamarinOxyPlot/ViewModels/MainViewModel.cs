﻿using System.Windows.Input;
using XamarinOxyPlot.Services.Navigation;
using XamarinOxyPlot.ViewModels.Base;

namespace XamarinOxyPlot.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private INavigationService _navigationService;

        public MainViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ICommand TimelineSampleCommand
        {
            get { return new DelegateCommand(TimelineSampleExecute); }
        }

        private void TimelineSampleExecute()
        {
            _navigationService.NavigateTo<TimelineSampleViewModel>();
        }
    }
}
