﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Charts;
using XamarinOxyPlot.Charts.DateTimeHeader;
using XamarinOxyPlot.Data;
using XamarinOxyPlot.ViewModels.Base;

namespace XamarinOxyPlot.ViewModels
{
    public class TimelineSampleViewModel : ViewModelBase
    {
        private readonly LocalDataService _dataService;

        private TimelinePlotModel _plotModel = new TimelinePlotModel();

        public TimelineSampleViewModel(LocalDataService dataService)
        {
            _plotModel = new TimelinePlotModel
            {
                Style = new ChartStyle
                {
                    DateTimeHeader = new HeaderStyle
                    {

                    },

                    LineNamesAxis = new LineNamesAxisStyle
                    {

                    }
                }
            };

            _dataService = dataService;
            Initialize();
        }

        public TimelinePlotModel PlotModel
        {
            get
            {
                return _plotModel;
            }

            set
            {
                _plotModel = value;
                RaisePropertyChanged();
            }
        }

        private async void Initialize()
        {
            ChartData result = await _dataService.LoadData();
            PlotModel.ChartData = result;
            
        }
    }
}
