﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class PointsSeries : LineSeries, IChartDataLine
    {
        private double _actualViewportXRange;

        private List<PointsZoomLayer> _zoomLayers;

        public Dictionary<double, List<DataPoint>> _preparedData = new Dictionary<double, List<DataPoint>>();

        public PointsSeries()
        {
            LineStyle = LineStyle.Solid;
            OriginalPoints = new List<DataPoint>();
            UpdatePoints(OriginalPoints);
            ZoomLayers = new List<PointsZoomLayer>()
            {
                new PointsZoomLayer
                {
                    Points = OriginalPoints,
                    IsReady = true,
                    OriginalSeries = this,
                }
            };

            UpdateDataPointsAccordingToScale();
        }

        public int LineIndex { get; set; }

        public List<DataPoint> OriginalPoints { get; protected set; }

        public List<PointsZoomLayer> ZoomLayers
        {
            get
            {
                return _zoomLayers;
            }

            set
            {
                if (_zoomLayers != value)
                {
                    _zoomLayers = value;
                    UpdateDataPointsAccordingToScale();
                }
            }
        }

        public double ActualViewportXRange
        {
            get
            {
                return _actualViewportXRange;
            }

            set
            {
                if (_actualViewportXRange != value)
                {
                    _actualViewportXRange = value;
                    UpdateDataPointsAccordingToScale();
                }
            }
        }

        public override void Render(IRenderContext rc)
        {
            if (XAxis == null)
            {
                return;
            }

            double viewportXRange = XAxis.ActualMaximum - XAxis.ActualMinimum;
            this.ActualViewportXRange = viewportXRange;

            LineNamesAxis lineNameAxis = this.YAxis as LineNamesAxis;
            if (lineNameAxis != null)
            {
                bool isInViewport = lineNameAxis.IsLineInViewport(this);
                if (isInViewport)
                {
                    EnsureDowsampledLayerIsReady();
                }
                else
                {
                    return;
                }

            }

            base.Render(rc);
        }

        public void EnsureDowsampledLayerIsReady()
        {
            PointsZoomLayer actualZoomLevel = ZoomLayers.Last();
            foreach (PointsZoomLayer zoomLayer in ZoomLayers)
            {
                if (_actualViewportXRange < zoomLayer.AcceptableViewportXRange)
                {
                    actualZoomLevel = zoomLayer;
                    break;
                }
            }


            actualZoomLevel.WaitWhenReady();
            UpdatePoints(actualZoomLevel.Points);
        }


        private void UpdateDataPointsAccordingToScale()
        {
            if (ActualViewportXRange == 0 ||
                ZoomLayers.Count == 0)
            {
                UpdatePoints(OriginalPoints);
                return;
            }

            PointsZoomLayer newZoomLayer = null;
            foreach (PointsZoomLayer zoomLayer in ZoomLayers)
            {
                if (_actualViewportXRange < zoomLayer.AcceptableViewportXRange && 
                    zoomLayer.IsReady)
                {
                    newZoomLayer = zoomLayer;
                    break;
                }
            }

            if (newZoomLayer == null)
            {
                newZoomLayer = ZoomLayers.FirstOrDefault(z => z.IsReady);
            }

            UpdatePoints(newZoomLayer.Points);
        }

        private void UpdatePoints(List<DataPoint> points)
        {
            if (Points != points)
            {
                Points = points;
                this.WindowStartIndex = 0;
            }
        }

        protected override void UpdateMaxMin()
        {
            if (OriginalPoints == null || OriginalPoints.Count == 0)
            {
                return;
            }

            this.MinX = this.OriginalPoints.First().X;
            this.MaxX = this.OriginalPoints.Last().X;
        }
    }
}
