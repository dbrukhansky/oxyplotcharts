﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class LineNamesAxisStyle
    {
        public LineNamesAxisStyle()
        {
            CellWidth = 120;
            LineHeight = 30;
            LineSpacing = 20;
            CellBackgrounds = new List<OxyColor>
            {
                OxyColor.Parse("#FFF6F8FB"),
                OxyColors.Transparent,
            };

            NameStyle = new TextStyle
            {
                TextColor = OxyColor.Parse("#FF263A51"),
                VerticalAlignment = VerticalAlignment.Middle,
                HorizontalAlignment = HorizontalAlignment.Left,
                FontSize = 14,
                TextTrimming = TrimmingMode.CharacterEllipsis,
            };
        }

        public double LineHeight { get; set; }

        public double LineSpacing { get; set; }

        public double CellWidth { get; set; }

        public List<OxyColor> CellBackgrounds { get; set; }

        public TextStyle NameStyle { get; set; }
    }
}
