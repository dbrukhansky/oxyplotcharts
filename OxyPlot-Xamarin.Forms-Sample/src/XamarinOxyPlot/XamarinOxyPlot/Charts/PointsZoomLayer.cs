﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class PointsZoomLayer
    {

        public double AcceptableViewportXRange { get; set; }

        public bool IsReady { get; set; }

        internal DownsamplingService DownsamplingService { get; set; }

        public int ExpectedPointsCount { get; set; }

        public PointsSeries OriginalSeries { get; set; }

        public List<DataPoint> Points { get; set; }

        public void WaitWhenReady()
        {
            DownsamplingService.WaitWhenReady(this);
        }
    }
}
