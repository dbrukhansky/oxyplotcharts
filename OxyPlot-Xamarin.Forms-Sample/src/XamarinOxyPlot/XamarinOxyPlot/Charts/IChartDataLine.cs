﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public interface IChartDataLine
    {
        string Title { get; }

        int LineIndex { get; set; }
    }
}
