﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class RangeBarItem : IntervalBarItem
    {
        public PreparedText PreparedText { get; set; }
    }
}
