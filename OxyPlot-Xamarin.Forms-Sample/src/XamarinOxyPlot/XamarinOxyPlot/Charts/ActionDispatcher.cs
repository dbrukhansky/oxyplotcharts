﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class ActionDispatcher<T>
    {
        private List<PendingAction<T>> _pendingOperations = new List<PendingAction<T>>();

        public void Enqueue(Action action, Func<bool> when)
        {
            Enqueue((rc) => action.Invoke(), when);
        }

        public void Enqueue(Action<T> action, Func<bool> when)
        {
            PendingAction<T> pendingItem = new PendingAction<T>
            {
                Action = action,
                CanExecutePredicate = when,
            };

            lock (_pendingOperations)
            {
                _pendingOperations.Add(pendingItem);
            }
        }

        public void Execute(T parameter)
        {
            List<PendingAction<T>> executedActions = null;
            lock (_pendingOperations)
            {
                for (int i = 0; i < _pendingOperations.Count; i++)
                {
                    var operation = _pendingOperations[i];
                    bool canExecute = operation.CanExecutePredicate.Invoke();
                    if (canExecute)
                    {
                        _pendingOperations.RemoveAt(i);
                        i--;
                        if (executedActions == null)
                        {
                            executedActions = new List<PendingAction<T>>();
                        }

                        executedActions.Add(operation);
                    }
                }
            }

            if (executedActions != null)
            {
                foreach (var action in executedActions)
                {
                    action.Action.Invoke(parameter);
                }
            }
        }

        private class PendingAction<T>
        {
            public Action<T> Action { get; set; }

            public Func<bool> CanExecutePredicate { get; set; }
        }

    }
}
