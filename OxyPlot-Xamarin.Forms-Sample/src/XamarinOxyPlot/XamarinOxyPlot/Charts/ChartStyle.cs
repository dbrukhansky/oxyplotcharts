﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Charts.DateTimeHeader;

namespace XamarinOxyPlot.Charts
{
    public class ChartStyle
    {
        public ChartStyle()
        {
            LineNamesAxis = new LineNamesAxisStyle();
            DateTimeHeader = new HeaderStyle();
        }

        public LineNamesAxisStyle LineNamesAxis { get; set; }

        public HeaderStyle DateTimeHeader { get; set; }
    }
}