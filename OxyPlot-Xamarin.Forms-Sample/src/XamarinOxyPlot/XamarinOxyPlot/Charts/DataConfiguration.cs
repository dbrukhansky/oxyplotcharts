﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class DataConfiguration
    {
        public DataConfiguration()
        {
            TimelinePointsMaxValue = Double.NaN;
            TimelinePointsMinValue = 0;
        }

        public double TimelinePointsMinValue { get; set; }

        public double TimelinePointsMaxValue { get; set; }
    }
}
