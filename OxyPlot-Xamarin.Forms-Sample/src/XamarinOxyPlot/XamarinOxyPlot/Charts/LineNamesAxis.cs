﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Data;

namespace XamarinOxyPlot.Charts
{
    public class LineNamesAxis : CategoryAxis
    {

        private PointsScalingParameters _pointsScaling;

        public LineNamesAxis()
        {
            Style = new LineNamesAxisStyle();
            // TODO: add line names.
            Position = AxisPosition.Left;
            Minimum = 0;
            Maximum = 20;
            IsZoomEnabled = false;
        }

        public LineNamesAxisStyle Style { get; set; }

        internal double LineHeight
        {
            get
            {
                return Style.LineHeight;
            }
        }

        internal double LineSpacing
        {
            get
            {
                return Style.LineSpacing;
            }
        }

        internal double LineTotalHeight
        {
            get
            {
                return LineHeight + LineSpacing;
            }
        }

        internal double LinesCountInViewport
        {
            get
            {
                return PlotModel.PlotArea.Height / LineTotalHeight;
            }
        }

        public double ConvertToReal(double value)
        {
            return _pointsScaling.ConvertToReal(value);
        }

        public double ConvertToChartScaledValue(double realValue, int timelineIndex)
        {
            return _pointsScaling.ConvertToChartScaledValue(realValue, timelineIndex);
        }

        public void Arrange(ChartData data, DataConfiguration configuration)
        {
            _pointsScaling = DetermineTimelinePointsScale(data, configuration, _pointsScaling);
        }

        public void MeasureYBounds(ChartData data)
        {
            //TODO: measure Y bounds according to Width, required height of line, etc.
            var cellingMaxVisibleLinesCount = Math.Ceiling(LinesCountInViewport);

            AbsoluteMinimum = 0;
            AbsoluteMaximum = Math.Max(data.Lines.Count, cellingMaxVisibleLinesCount);
            if (AbsoluteMaximum != data.Lines.Count)
            {
                AbsoluteMinimum = AbsoluteMaximum - LinesCountInViewport;
            }

            var linesCount = LinesCountInViewport;
            var minimum = AbsoluteMaximum - linesCount;
            Minimum = minimum;
            Maximum = AbsoluteMaximum;
            Reset();
        }

        private PointsScalingParameters DetermineTimelinePointsScale(ChartData data, DataConfiguration configuration, PointsScalingParameters previousInfo)
        {
            PointsScalingParameters newData = DetermineTimelinePointsScale(data, configuration);
            PointsScalingParameters result = newData;
            if (previousInfo != null)
            {
                double maxValue = Math.Max(previousInfo.MaxValue, newData.MaxValue);
                double minValue = Math.Min(previousInfo.MinValue, newData.MinValue);
                int index = DetermineMaxLineIndex(data);
                result = new PointsScalingParameters(index, minValue, maxValue);
            }

            return result;
        }

        private PointsScalingParameters DetermineTimelinePointsScale(ChartData data, DataConfiguration configuration)
        {
            double minValue = 0;
            double maxValue = 0;
            IEnumerable<double> allPointValues = data.Lines.Where(l => l is PointsLine)
                                                           .SelectMany(l => ((PointsLine)l).Points)
                                                           .Select(p => p.Value);
            if (allPointValues.Any())
            {
                minValue = allPointValues.Min();
                maxValue = allPointValues.Max();

                //TODO: use spacing here.

                var lineSpacingRatio = LineSpacing / (2 * LineTotalHeight); 

                double spacingPlace = (maxValue - minValue) * lineSpacingRatio;
                maxValue += spacingPlace;
                minValue -= spacingPlace;
            }

            if (!Double.IsNaN(configuration.TimelinePointsMinValue))
            {
                minValue = Math.Min(minValue, configuration.TimelinePointsMinValue);
            }

            if (!Double.IsNaN(configuration.TimelinePointsMaxValue))
            {
                maxValue = Math.Max(configuration.TimelinePointsMaxValue, maxValue);
            }

            if (minValue >= maxValue)
            {
                maxValue = minValue + 1;
            }

            int index = DetermineMaxLineIndex(data);
            return new PointsScalingParameters(index, minValue, maxValue);
        }

        public override OxySize Measure(IRenderContext rc)
        {
            return this.DesiredSize = new OxySize(Style.CellWidth, 0);
        }

        private int DetermineMaxLineIndex(ChartData data)
        {
            int maxVisibleLinesCount = (int)Math.Ceiling(LinesCountInViewport);
            int lineIndex = Math.Max(data.Lines.Count - 1, maxVisibleLinesCount - 1);
            return lineIndex;
        }

        public bool IsLineInViewport(IChartDataLine chartDataLine)
        {
            var lineBottomY = ConvertToChartScaledValue(0, chartDataLine.LineIndex);

            if (lineBottomY > ActualMaximum)
            {
                return false;
            }

            var lineTopY = ConvertToChartScaledValue(0, chartDataLine.LineIndex - 1);
            double maximum = Double.IsNaN(ViewMinimum) ? Minimum : ViewMinimum;
            if (lineTopY < ActualMinimum)
            {
                return false;
            }

            return true;
            
        }

        public override void Render(IRenderContext rc, int pass)
        {
            if (pass != 0)
            {
                return;
            }

            //var minTimelineIndex = ConvertToReal1(this.ActualMaximum);
            //var maxTimelineIndex = ConvertToReal1(this.ActualMinimum);

            //maxTimelineIndex--;
            //minTimelineIndex++;

            OxyRect axisRect = new OxyRect(PlotModel.PlotAndAxisArea.Left, PlotModel.PlotArea.Top, Style.CellWidth, PlotModel.PlotArea.Height);

            rc.SetClip(axisRect);

            try
            {
                foreach (Series series in PlotModel.Series)
                {
                    IChartDataLine dataLine = (IChartDataLine)series;
                    //if (series.LineIndex >= maxTimelineIndex || 
                    //    series.LineIndex <= minTimelineIndex)
                    //{
                    //    continue;
                    //}

                    var lineBottom = (int)ConvertToChartScaledValue(0, dataLine.LineIndex);
                    var lineTop = (int)ConvertToChartScaledValue(0, dataLine.LineIndex - 1);
                    if (lineBottom > this.ActualMaximum ||
                        lineTop < this.ActualMinimum)
                    {
                        continue;
                    }

                    var screenLineBottom = Transform(lineBottom);
                    var screenLineTop = Transform(lineTop);

                    OxyRect titleCellRect = new OxyRect(PlotModel.PlotAndAxisArea.Left,
                                                        screenLineTop,
                                                        PlotModel.PlotArea.Left - PlotModel.PlotAndAxisArea.Left,
                                                        screenLineBottom - screenLineTop);

                    OxyColor background = OxyColors.Transparent;
                    if (Style.CellBackgrounds.Count > 0)
                    {
                        int colorIndex = dataLine.LineIndex % Style.CellBackgrounds.Count;
                        background = Style.CellBackgrounds[colorIndex];
                    }
                   
                    rc.DrawRectangle(titleCellRect, background, OxyColors.Transparent);

                    var textRect = new OxySize(titleCellRect.Width - 10, titleCellRect.Height);
                    var textPosition = new ScreenPoint(titleCellRect.Left + 5, titleCellRect.Center.Y);

                    rc.DrawText(textPosition,
                                series.Title,
                                Style.NameStyle,
                                maxSize: textRect);
                }
            }
            finally
            {
                rc.ResetClip();
            }
        }
    }

    internal class PointsScalingParameters
    {
        private double _scale;

        public PointsScalingParameters(int initialLineIndex,  double minValue, double maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            InitialLineIndex = initialLineIndex;
            _scale = MaxValue - MinValue;
        }

        public double MinValue { get; private set; }

        public double MaxValue { get; private set; }

        public int InitialLineIndex { get; private set; }

        public double ConvertToReal(double value)
        {
            int timelineIndex = (int)(Math.Truncate(value));
            double fraction = value - timelineIndex;
            double realValue = fraction * _scale + MinValue;
            return realValue;
        }

        public double ConvertToChartScaledValue(double realValue, int timelineIndex)
        {
            var convertedIndex = InitialLineIndex - timelineIndex; 
            double scaledValue = (realValue - MinValue) / _scale;
            return convertedIndex + scaledValue;
        }
    }
}
