﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinOxyPlot.Charts
{
    internal class DownsamplingService
    {
        private int _maxVisiblePointsPerLine;

        private OxySize _chartAreaSize;

        private List<PointsZoomLayer> _pendingZoomLayers = new List<PointsZoomLayer>();

        private readonly object queueLock = new object();

        private readonly object _processingLock = new object();

        private bool _isProcessedInBackground;


        public DownsamplingService()
        {
            //TODO: value can be scaled according to device screen size, rendering performance, etc.
            MaxPointsCount = 6000;
        }

        public int MaxPointsCount { get; set; }

        public void Configure(double lineHeight, OxySize chartAreaSize)
        {
            int maxVisibleLinesCount = (int)Math.Ceiling(chartAreaSize.Height / lineHeight) + 1;
            _maxVisiblePointsPerLine = MaxPointsCount / maxVisibleLinesCount;
        }

        public void CreateZoomLayers(PointsSeries lineSeries)
        {
            List<DataPoint> points = lineSeries.OriginalPoints;
            double minX = points.First().X;
            double maxX = points.Last().X;
            double xRange = maxX - minX;

            //TODO: find better solution for processing lines with gaps. 
            List<DataPoint> evenLine = points; // GenerateIntermediatePoints(points, 0.00001);
            double pointsPerPixel = evenLine.Count / _chartAreaSize.Width;
            List<PointsZoomLayer> downsapledLines = new List<PointsZoomLayer>();
            int requiredCount = evenLine.Count;

            while (true)
            {
                double pointsDensity = requiredCount / xRange;
                double accaptableXRange = _maxVisiblePointsPerLine / pointsDensity;

                PointsZoomLayer line = new PointsZoomLayer
                {
                    DownsamplingService = this,
                    OriginalSeries = lineSeries,
                    ExpectedPointsCount = requiredCount,
                    AcceptableViewportXRange = accaptableXRange,
                };

                if (evenLine.Count == requiredCount)
                {
                    line.IsReady = true;
                    line.Points = evenLine;
                }

                downsapledLines.Add(line);

                if (requiredCount < _maxVisiblePointsPerLine)
                {
                    break;
                }

                requiredCount = (int)(requiredCount / 2);
                pointsPerPixel *= requiredCount;
            }

            lineSeries.ZoomLayers = downsapledLines;
            lock (queueLock)
            {
                _pendingZoomLayers.AddRange(downsapledLines.Where(l => !l.IsReady));
            }

            ProcessDownsampledData();
        }

        private void ProcessDownsampledData()
        {
            lock (queueLock)
            {
                if (_isProcessedInBackground)
                {
                    return;
                }

                _isProcessedInBackground = true;
            }

            Task.Run(() =>
            {
                while (true)
                {
                    PointsZoomLayer processingLayer = null;
                    lock (queueLock)
                    {
                        if (_pendingZoomLayers.Count > 0)
                        {
                            processingLayer = _pendingZoomLayers[0];
                            _pendingZoomLayers.RemoveAt(0);
                        }
                        else
                        {
                            _isProcessedInBackground = false;
                            break;
                        }
                    }

                    DownsampleLayer(processingLayer);
                }
            });
        }

        public void WaitWhenReady(PointsZoomLayer layer)
        {
            if (layer.IsReady)
            {
                return;
            }

            lock (queueLock)
            {
                _pendingZoomLayers.Remove(layer);
            }

            DownsampleLayer(layer);
        }

        public void DownsampleLayer(PointsZoomLayer zoomLayer)
        {
            var downsampledData = Downsample(zoomLayer.OriginalSeries.OriginalPoints, zoomLayer.ExpectedPointsCount);
            zoomLayer.Points = downsampledData;
            zoomLayer.IsReady = true;
        }

        private static List<DataPoint> GenerateIntermediatePoints(List<DataPoint> data, double differenceLimit)
        {
            if (data.Count <= 2)
            {
                return new List<DataPoint>(data);
            }

            List<DataPoint> resultList = new List<DataPoint>(data);
            double minInteval = FindMinInterval(resultList, differenceLimit);
            for (int i = 0; i < resultList.Count - 1; i++)
            {
                DataPoint leftItem = resultList[i];
                DataPoint rightItem = resultList[i + 1];
                double currentInterval = rightItem.X - leftItem.X;
                if (currentInterval > minInteval * 2)
                {
                    double dy = (rightItem.Y - leftItem.Y) / (rightItem.X - leftItem.X);
                    double insertedPointX = leftItem.X + minInteval;
                    while (insertedPointX < rightItem.X - minInteval)
                    {
                        double insertedPointY = leftItem.Y + dy * (insertedPointX - leftItem.X);
                        i++;
                        DataPoint newDataPoint = new DataPoint(insertedPointX, insertedPointY);
                        resultList.Insert(i, newDataPoint);
                        insertedPointX += minInteval;
                    }
                }
            }

            return resultList;
        }

        private static double FindMinInterval(List<DataPoint> data, double intervalLimit)
        {
            double minInterval = double.MaxValue;
            for (int i = 0; i < data.Count - 1; i++)
            {
                var leftItem = data[i];
                var rightItem = data[i + 1];
                double interval = rightItem.X - leftItem.X;
                if (minInterval > interval &&
                    interval > intervalLimit)
                {
                    minInterval = interval;
                }
            }

            if (minInterval == double.MaxValue)
            {
                minInterval = intervalLimit;
            }

            return minInterval;
        }


        private static List<DataPoint> Downsample(List<DataPoint> data, int pointsCount)
        {
            return LargestTriangleThreeBuckets(data, pointsCount);
        }

        //TODO: move to separate class.
        public static List<DataPoint> LargestTriangleThreeBuckets(List<DataPoint> data, int threshold)
        {
            // Implementation of  Largest-Triangle-Three-Buckets algorithm was taked here: https://gist.github.com/DanielWJudge/63300889f27c7f50eeb7
            // See http://flot.base.is/ for sample and information.

            int dataLength = data.Count;
            if (threshold >= dataLength || threshold == 0)
                return data; // Nothing to do

            List<DataPoint> sampled = new List<DataPoint>(threshold);

            // Bucket size. Leave room for start and end data points
            double every = (double)(dataLength - 2) / (threshold - 2);

            int a = 0;
            DataPoint maxAreaPoint = new DataPoint(0, 0);
            int nextA = 0;

            sampled.Add(data[a]); // Always add the first point
            for (int i = 0; i < threshold - 2; i++)
            {
                // Calculate point average for next bucket (containing c)
                double avgX = 0;
                double avgY = 0;
                int avgRangeStart = (int)(Math.Floor((i + 1) * every) + 1);
                int avgRangeEnd = (int)(Math.Floor((i + 2) * every) + 1);
                avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;

                int avgRangeLength = avgRangeEnd - avgRangeStart;

                for (; avgRangeStart < avgRangeEnd; avgRangeStart++)
                {
                    avgX += data[avgRangeStart].X; // * 1 enforces Number (value may be Date)
                    avgY += data[avgRangeStart].Y;
                }
                avgX /= avgRangeLength;

                avgY /= avgRangeLength;

                // Get the range for this bucket
                int rangeOffs = (int)(Math.Floor((i + 0) * every) + 1);
                int rangeTo = (int)(Math.Floor((i + 1) * every) + 1);

                // Point a
                double pointAx = data[a].X; // enforce Number (value may be Date)
                double pointAy = data[a].Y;

                double maxArea = -1;

                for (; rangeOffs < rangeTo; rangeOffs++)
                {
                    // Calculate triangle area over three buckets
                    double area = Math.Abs((pointAx - avgX) * (data[rangeOffs].Y - pointAy) -
                                           (pointAx - data[rangeOffs].X) * (avgY - pointAy)
                                      ) * 0.5;
                    if (area > maxArea)
                    {
                        maxArea = area;
                        maxAreaPoint = data[rangeOffs];
                        nextA = rangeOffs; // Next a is this b
                    }
                }

                sampled.Add(maxAreaPoint); // Pick this point from the bucket
                a = nextA; // This a is the next a (chosen b)
            }

            sampled.Add(data[dataLength - 1]); // Always add last

            return sampled;
        }
    }
}
