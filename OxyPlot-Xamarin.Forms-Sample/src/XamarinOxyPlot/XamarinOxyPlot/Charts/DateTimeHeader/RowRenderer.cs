﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public abstract class RowRenderer
    { 
        public RenderingOrder OrderIndex { get; set; }

        public virtual double MeasureMinCellWidth(IRenderContext rc)
        {
            return 0;
        }

        public abstract void Draw(HeaderRow row, IRenderContext rc);
    }

    
}
