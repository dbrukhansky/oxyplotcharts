﻿using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class DateTimeInterval
    {
        private static readonly DateTime ZeroDate = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc); // DateTimeAxis.ToDateTime(0);

        public DateTimeInterval(int count, DateTimeIntervalType intervalType)
        {
            Count = count;
            IntervalType = intervalType;
            switch (IntervalType)
            {
                case DateTimeIntervalType.Milliseconds:
                    MinValue = MaxValue = TimeSpan.FromMilliseconds(1);
                    break;

                case DateTimeIntervalType.Seconds:
                    MinValue = MaxValue = TimeSpan.FromSeconds(1);
                    break;

                case DateTimeIntervalType.Minutes:
                    MinValue = MaxValue = TimeSpan.FromMinutes(1);
                    break;

                case DateTimeIntervalType.Hours:
                    MinValue = MaxValue = TimeSpan.FromHours(1);
                    break;

                case DateTimeIntervalType.Days:
                    MinValue = MaxValue = TimeSpan.FromDays(1);
                    break;

                case DateTimeIntervalType.Weeks:
                    MinValue = MaxValue = TimeSpan.FromDays(7);
                    break;

                case DateTimeIntervalType.Months:
                    MinValue = TimeSpan.FromDays(28);
                    MaxValue = TimeSpan.FromDays(31);
                    break;

                case DateTimeIntervalType.Years:
                    MinValue = TimeSpan.FromDays(365);
                    MaxValue = TimeSpan.FromDays(366);
                    break;
            }

            MinValue = TimeSpan.FromTicks(MinValue.Ticks * Count);
            MaxValue = TimeSpan.FromTicks(MaxValue.Ticks * Count);
        }

        public DateTimeInterval Multiply(int value)
        {
            return new DateTimeInterval(Count * value, IntervalType);
        }

        public int Count { get; private set; }

        public DateTimeIntervalType IntervalType { get; private set; }

        public TimeSpan MinValue { get; private set; }

        public TimeSpan MaxValue { get; private set; }

        public DateTime RoundDateTime(DateTime date)
        {
            DateTime result = date;
            switch (IntervalType)
            {
                case DateTimeIntervalType.Months:
                    result = RoundMonth(date);
                    break;

                case DateTimeIntervalType.Years:
                    result = RoundYear(date);
                    break;


                default:
                    result = RoundTicks(date);
                    break;
            }

            return result;
        }

        private DateTime RoundMonth(DateTime date)
        {
            var totalMonthCount = (date.Year - ZeroDate.Year) * 12 + (date.Month - ZeroDate.Month);
            var integerPart = totalMonthCount / Count;
            var roundedMonthCount = integerPart * Count;
            int yearsCount = roundedMonthCount / 12;
            int monthIndex = roundedMonthCount % 12 + 1;
            if (monthIndex <= 0)
            {
                monthIndex += 12;
                yearsCount--;
            }

            return new DateTime(ZeroDate.Year + yearsCount, monthIndex, 1, 0, 0, 0, ZeroDate.Kind);
        }

        private DateTime RoundYear(DateTime date)
        {
            var totalYearsCount = date.Year - ZeroDate.Year;
            var integerPart = totalYearsCount / Count;

            var remainingPart = totalYearsCount % Count;
            if (remainingPart < 0)
            {
                remainingPart += Count;
                integerPart--;
            }

            var roundedYearsCount = integerPart * Count;
            return new DateTime(ZeroDate.Year + roundedYearsCount, 1, 1, 0, 0, 0, ZeroDate.Kind);
        }

        private DateTime RoundTicks(DateTime date)
        {
            var difference = date - ZeroDate;
            var integerPart = difference.Ticks / MinValue.Ticks;
            var roundedTicks = integerPart * MinValue.Ticks;
            return ZeroDate + TimeSpan.FromTicks(roundedTicks);
        }

        public DateTime AddTo(DateTime dateTime)
        {
            DateTime result = dateTime;
            switch (IntervalType)
            {
                case DateTimeIntervalType.Months:
                    result = dateTime.AddMonths(Count);
                    break;

                case DateTimeIntervalType.Years:
                    result = dateTime.AddYears(Count);
                    break;

                default:
                    result = dateTime + MinValue;
                    break;
            }

            return result;
        }
    }
}
