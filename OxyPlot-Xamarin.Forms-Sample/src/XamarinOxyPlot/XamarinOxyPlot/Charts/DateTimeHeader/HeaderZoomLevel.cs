﻿using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class HeaderZoomLevel
    { 

        public HeaderZoomLevel()
        {
            MinCellWidth = 60;
            Renderers = new List<RowRenderer>();
        }

        public double MinCellWidth { get; set; }

        public DateTimeInterval CellStep { get; set; }

        public RowRenderer RowRenderer { get; set; }

        public List<RowRenderer> Renderers { get; set; }

        public bool IsAccessoryLevel { get; set; }
    }
}
