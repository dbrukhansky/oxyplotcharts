﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class ColorResources
    {
        static ColorResources()
        {
            DarkBlueColor = OxyColor.Parse("#FF8394B6");
        }

        public static OxyColor DarkBlueColor { get; set; } 
    }

    public class HeaderStyle
    {
        public HeaderStyle()
        {
            BackgroundColor = ColorResources.DarkBlueColor;
            FirstRow = new FirstHeaderRowStyle();
            SecondRow = new SecondRowStyle();
        }

        public OxyColor BackgroundColor { get; set; }

        public FirstHeaderRowStyle FirstRow { get; private set; }

        public SecondRowStyle SecondRow { get; private set; }
    }

    public class RowStyle
    {
        public OxyPen PlotAreaSeparators { get; set; }

        public OxyPen CellSeparators { get; set; }
    }

    public class FirstHeaderRowStyle : RowStyle
    {
        public FirstHeaderRowStyle()
        {
            PlotAreaSeparators = new OxyPen(OxyColor.Parse("#FFD8DDE5"), 2);
            CellSeparators = new OxyPen(OxyColor.Parse("#FFB5C2DA"), 1);
            LabelStyle = new TextStyle
            {
                TextColor = OxyColors.White,
                FontSize = 18,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Middle,
            };
        }

        public TextStyle LabelStyle { get; set; }
    }

    public class SecondRowStyle : RowStyle
    {
        public SecondRowStyle()
        {
            PlotAreaSeparators = new OxyPen(OxyColor.Parse("#FFE5E6EA"), 1);
            CellSeparators = new OxyPen(OxyColor.Parse("#FF8394B6"), 1);
            WeekendStyles = new WeekendStyles();
            TopLabelStyle = new TextStyle
            {
                TextColor = OxyColors.White,
                FontSize = 14,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Middle,
            };

            BottomLabelStyle = new TextStyle
            {
                TextColor = OxyColor.Parse("#FF263A51"),
                FontSize = 11,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Middle,
            };
        }

        public TextStyle TopLabelStyle { get; set; }

        public TextStyle BottomLabelStyle { get; set; }


        public WeekendStyles WeekendStyles { get; set; }

    }

    public class WeekendStyles
    {
        public WeekendStyles()
        {
            PlotAreaHighlightColor = OxyColor.Parse("#FFF1F5F9");
            TopLabelHighlightColor = OxyColor.Parse("#FF91A1BF");
            BottomLabelHighlightColor = OxyColor.Parse("#FF95B7DF");
        }

        public OxyColor PlotAreaHighlightColor { get; set; }

        public OxyColor TopLabelHighlightColor { get; set; }

        public OxyColor BottomLabelHighlightColor { get; set; }
    }
}
