﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class SecondRowRenderer : RowRenderer
    {
        private string _topTextFormat;

        private string _bottomTextFormat;

        private Func<DateTime, string> _topTextEvaluator = (date) => string.Empty;

        private Func<DateTime, string> _bottomTextEvaluator = (date) => string.Empty;

        public override double MeasureMinCellWidth(IRenderContext rc)
        {
            return 50;
        }

        public string TopTextFormat
        {
            get
            {
                return _topTextFormat;
            }

            set
            {
                _topTextFormat = value;
                if (_topTextFormat != null)
                {
                    _topTextEvaluator = ConvertTopText;
                }
            }
        }

        public string BottomTextFormat
        {
            get
            {
                return _bottomTextFormat;
            }

            set
            {
                _bottomTextFormat = value;
                if (_bottomTextFormat != null)
                {
                    _bottomTextEvaluator = ConvertBottomText;
                }
            }
        }

        private string ConvertTopText(DateTime date)
        {
            return date.ToString(_topTextFormat, CultureInfo.InvariantCulture);
        }

        private string ConvertBottomText(DateTime date)
        {
            return date.ToString(_bottomTextFormat, CultureInfo.InvariantCulture);
        }

        public override void Draw(HeaderRow row, IRenderContext rc)
        {
            var rowStyle = (SecondRowStyle)row.Style;
            var axis = row.Axis;
            var padding = new OxyThickness(1);
            OxyPen lineStyle = row.Style.PlotAreaSeparators;
            foreach (var cell in row.Cells)
            {
                var rectLeft = axis.Transform(cell.Start) + padding.Left;
                var rectRight = axis.Transform(cell.End) - padding.Right;
                var middleY = row.RowArea.Top + row.RowArea.Height * 0.5;
                var firstRectTop = row.RowArea.Top + padding.Top;
                var firstRectBottom = middleY - padding.Bottom;
                var secondRectTop = middleY + padding.Top;
                var secondRectBottom = row.RowArea.Bottom - padding.Bottom;

                string topContent = _topTextEvaluator.Invoke(cell.StartDate);
                string bottomContent = _bottomTextEvaluator.Invoke(cell.StartDate);
                if (String.IsNullOrEmpty(bottomContent))
                {
                    //Hide second row.
                    firstRectBottom = secondRectBottom;
                }


                var topRect = new OxyRect(rectLeft, firstRectTop, rectRight - rectLeft, firstRectBottom - firstRectTop);
                var bottomRect = new OxyRect(rectLeft, secondRectTop, rectRight - rectLeft, secondRectBottom - secondRectTop);

                rc.DrawText(topRect.Center,
                            topContent,
                            rowStyle.TopLabelStyle,
                            maxSize: new OxySize(topRect.Width, topRect.Height)
                            );

                if (!String.IsNullOrEmpty(bottomContent))
                {

                    rc.DrawText(bottomRect.Center,
                                bottomContent,
                                rowStyle.BottomLabelStyle,
                                maxSize: new OxySize(bottomRect.Width, bottomRect.Height)
                                );
                }
            }
        }
    }
}
