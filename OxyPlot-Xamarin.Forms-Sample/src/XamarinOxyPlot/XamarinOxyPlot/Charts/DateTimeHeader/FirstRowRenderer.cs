﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class FirstRowRenderer : RowRenderer
    {
        private string _dateFormat;

        private Func<DateTime, string> _headerContentAction = (date) => string.Empty;

        public string DateFormat
        {
            get
            {
                return _dateFormat;
            }

            set
            {
                _dateFormat = value;
                if (_dateFormat != null)
                {
                    _headerContentAction = ConvertDate;
                }
            }
        }

        public override double MeasureMinCellWidth(IRenderContext rc)
        {
            //TODO: measure longest date for different localizations.
            return 180;
        }

        private string ConvertDate(DateTime date)
        {
            return date.ToString(_dateFormat, CultureInfo.InvariantCulture);
        }

        public override void Draw(HeaderRow row, IRenderContext rc)
        {
            FirstHeaderRowStyle rowStyle = (FirstHeaderRowStyle)row.Style;
            var axis = row.Axis;
            var padding = new OxyThickness(1);
            OxyPen lineStyle = row.Style.PlotAreaSeparators;
            foreach (var cell in row.Cells)
            {
                var rectLeft = axis.Transform(cell.Start) + padding.Left;
                var rectRight = axis.Transform(cell.End) - padding.Right;
                var rectTop = row.RowArea.Top + padding.Top;
                var rectBottom = row.RowArea.Bottom - padding.Bottom;

                var rect = new OxyRect(rectLeft, rectTop, rectRight - rectLeft, rectBottom - rectTop);
                string content = _headerContentAction.Invoke(cell.StartDate);

                DateTimeAxis.ToDateTime(cell.Start);
                rc.DrawText(rect.Center,
                            content,
                            rowStyle.LabelStyle,
                            maxSize: new OxySize(rect.Width, rect.Height)
                            );
            }
        }
    }
}
