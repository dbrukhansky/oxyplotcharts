﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class WeekendsTracker
    {
        public bool IsWeekend(DateTime startDate, DateTime endDate)
        {
            int dayIndex = GetMondayTillSundayWeekDay(startDate.DayOfWeek);
            if (dayIndex < GetMondayTillSundayWeekDay(DayOfWeek.Saturday))
            {
                return false;
            }

            var daysCountTillNextMonday = 7 - dayIndex;
            var nextMonday = startDate.Date.AddDays(daysCountTillNextMonday);
            if (endDate <= nextMonday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int GetMondayTillSundayWeekDay(DayOfWeek dayOfWeek)
        {
            int dayIndex = (int)dayOfWeek;
            dayIndex--;
            if (dayIndex < 0)
            {
                dayIndex = 6;
            }

            return dayIndex;
        }
    }

}
