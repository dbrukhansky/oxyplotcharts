﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class HeaderCell
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public double Start { get; set; }

        public double End { get; set; }
    }
}
