﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public enum RenderingOrder
    {
        Default,
        FirstRowSeparatorLines,
        WeeksLayerOverrided,
    }
}
