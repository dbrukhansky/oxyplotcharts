﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class WeekendsRenderer : RowRenderer
    {
        private WeekendsTracker _weekendTracker = new WeekendsTracker();

        public override void Draw(HeaderRow row, IRenderContext rc)
        {
            double? weekendCellStart = null;
            double? weekendPeriodEnd = null;


            foreach (var cell in row.Cells)
            {
                bool isWeekend = _weekendTracker.IsWeekend(cell.StartDate, cell.EndDate);
                if (!weekendCellStart.HasValue)
                {
                    if (isWeekend)
                    {
                        weekendCellStart = cell.Start;
                        weekendPeriodEnd = cell.End;
                    }
                }
                else
                {
                    if (isWeekend)
                    {
                        weekendPeriodEnd = cell.End;
                    }
                    else
                    {
                        DrawWeekend(row, rc, weekendCellStart.Value, weekendPeriodEnd.Value);
                        weekendCellStart = null;
                        weekendPeriodEnd = null;
                    }
                }
            }

            if (weekendCellStart.HasValue)
            {
                DrawWeekend(row, rc, weekendCellStart.Value, weekendPeriodEnd.Value);
            }
        }

        private void DrawWeekend(HeaderRow row, IRenderContext rc, double start, double end)
        {
            var weekendStyles = row.Axis.Style.SecondRow.WeekendStyles;
            var axis = row.Axis;
            double weekendRectLeft = axis.Transform(start);
            double weekendRectRight = axis.Transform(end);
            double weekendRectWidth = weekendRectRight - weekendRectLeft;

            OxyRect plotArea = row.Axis.PlotModel.PlotArea;


            OxyRect plotWeekendRect = new OxyRect(weekendRectLeft, plotArea.Top, weekendRectWidth, plotArea.Height);

            OxyColor plotWeekendColor = weekendStyles.PlotAreaHighlightColor; 
            rc.DrawRectangle(plotWeekendRect, plotWeekendColor, OxyColors.Transparent);


            OxyColor firstLineBackground = weekendStyles.TopLabelHighlightColor; 

            OxyRect headerWeekendRect1 = new OxyRect(weekendRectLeft, row.RowArea.Top, weekendRectWidth, row.RowArea.Height / 2);
            rc.DrawRectangle(headerWeekendRect1, firstLineBackground, OxyColors.Transparent);

            OxyColor secondLineBackground = weekendStyles.BottomLabelHighlightColor; 
            var headerWeekendRect2 = new OxyRect(weekendRectLeft, row.RowArea.Center.Y, weekendRectWidth, row.RowArea.Height / 2);
            rc.DrawRectangle(headerWeekendRect2, secondLineBackground, OxyColors.Transparent);
        }
    }
}
