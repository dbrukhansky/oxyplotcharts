﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class WeekClearingBackgroundRenderer : RowRenderer
    {
        public override void Draw(HeaderRow row, IRenderContext rc)
        {
            
            var backgroundColor = row.Axis.Style.BackgroundColor;
            rc.DrawRectangle(row.RowArea, backgroundColor, OxyColors.Transparent);
        }
    }
}
