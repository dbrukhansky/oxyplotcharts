﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class HeaderRow
    {
        private static readonly DateTime ZeroDate = DateTimeAxis.ToDateTime(0);

        public HeaderRow()
        {
            Cells = new List<HeaderCell>();

        }

        public RowStyle Style { get; set; }

        public OxyRect RowArea { get; set; }

        public MultitiersDateTimeAxis Axis { get; set; }

        public List<HeaderCell> Cells { get; set; }

        public List<HeaderZoomLevel> ZoomLevels { get; set; }

        public HeaderZoomLevel ActualZoomLevel { get; set; }

        public void SwitchViewport(DateTimeInterval minInterval, DateTimeInterval maxInterval)
        {

        }


        public void UpdateActualZoomLevel(DateTimeInterval minInterval)
        {
            ActualZoomLevel = ZoomLevels.Last();

            foreach (HeaderZoomLevel zoomLevel in ZoomLevels)
            {
                if (minInterval != null)
                {
                    if (zoomLevel.CellStep.MinValue <= minInterval.MaxValue)
                    {
                        continue;
                    }
                }

                var minCellViewportWidth = DateTimeAxis.ToDouble(ZeroDate + zoomLevel.CellStep.MinValue);
                var actualCellWidth = Axis.Scale * minCellViewportWidth;
                if (actualCellWidth > zoomLevel.MinCellWidth)
                {
                    ActualZoomLevel = zoomLevel;
                    break;
                }
            }
        }

        public void TryApplyNonAccessoryZoomLevel(DateTimeInterval maxInterval)
        {
            int index = ZoomLevels.IndexOf(ActualZoomLevel);
            for (int i = index; i < ZoomLevels.Count; i++)
            {
                var zoomLevel = ZoomLevels[i];
                if (zoomLevel.CellStep.MaxValue >= maxInterval.MinValue)
                {
                    break;
                }

                if (!zoomLevel.IsAccessoryLevel)
                {
                    ActualZoomLevel = zoomLevel;
                    break;
                }
            }
        }

        public void UpdateCells(double actualMinimum, double actualMaximum)
        {
            Cells.Clear();
            DateTime minimumDate = DateTimeAxis.ToDateTime(actualMinimum);
            DateTime maximumDate = DateTimeAxis.ToDateTime(actualMaximum);

            DateTimeInterval cellSize = ActualZoomLevel.CellStep;
            DateTime minCellStartDate = minimumDate.Round(cellSize);
            DateTime maxCellEndDate = maximumDate.Round(cellSize).Add(cellSize);
            DateTime cellStart = minCellStartDate;
            while (cellStart < maxCellEndDate)
            {
                DateTime cellEnd = cellStart.Add(cellSize);
                HeaderCell cell = new HeaderCell
                {
                    Start = DateTimeAxis.ToDouble(cellStart),
                    End = DateTimeAxis.ToDouble(cellEnd),
                    StartDate = cellStart,
                    EndDate = cellEnd,
                };

                Cells.Add(cell);

                cellStart = cellEnd;
            }
        }
    }
}
