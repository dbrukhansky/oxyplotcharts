﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public static class DateTimeIntervalExtensions
    {
        public static DateTime Round(this DateTime dateTime, DateTimeInterval interval)
        {
            return interval.RoundDateTime(dateTime);
        }

        public static DateTime Add(this DateTime dateTime, DateTimeInterval interval)
        {
            return interval.AddTo(dateTime);
        }
    }
}
