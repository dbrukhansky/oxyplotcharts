﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class MultitiersDateTimeAxisRenderer
    {
        private IRenderContext _rc;

        private MultitiersDateTimeAxis _axis;

        private int _pass;

        public MultitiersDateTimeAxisRenderer(MultitiersDateTimeAxis axis, IRenderContext rc, int pass)
        {
            _rc = rc;
            _axis = axis;
            _pass = pass;
        }

        public void Render()
        {
            var backgroundColor = _axis.Style.BackgroundColor;

            //TODO: fix plot background size.
            var backgroundRect = new OxyRect(_axis.PlotModel.PlotAndAxisArea.Left, _axis.HeaderArea.Top, _axis.PlotModel.PlotAndAxisArea.Width, this._axis.HeaderArea.Height);
            _rc.DrawRectangle(backgroundRect, backgroundColor, OxyColors.Transparent, 0);

            var plotArea = _axis.PlotModel.PlotArea;
            _rc.DrawLine(plotArea.Left, _axis.HeaderArea.Top, plotArea.Left, _axis.HeaderArea.Bottom, _axis.Style.FirstRow.CellSeparators);
           var headerClipping = new OxyRect(this._axis.HeaderArea.Left, _axis.HeaderArea.Top, this._axis.HeaderArea.Width, this._axis.PlotModel.PlotAndAxisArea.Height);

            try
            {
                _rc.SetClip(headerClipping);

                var allRendererIndexes = _axis.HeaderRows.SelectMany(r => r.ActualZoomLevel.Renderers)
                                                         .Select(r => r.OrderIndex)
                                                         .Distinct()
                                                         .OrderBy(r => r);
                foreach (var index in allRendererIndexes)
                {
                    foreach (var row in _axis.HeaderRows)
                    {
                        var rowRenderers = row.ActualZoomLevel.Renderers;
                        foreach (var renderer in row.ActualZoomLevel.Renderers)
                        {
                            if (renderer.OrderIndex == index)
                            {
                                renderer.Draw(row, _rc);
                            }
                        }
                    }
                }
            }
            finally
            {
                _rc.ResetClip();
            }
        }
    }
}
