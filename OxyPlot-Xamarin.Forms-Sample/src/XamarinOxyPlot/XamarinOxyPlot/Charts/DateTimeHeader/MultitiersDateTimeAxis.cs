﻿using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using System.Diagnostics;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class MultitiersDateTimeAxis : DateTimeAxis
    {
        private static readonly DateTime ZeroDate = DateTimeAxis.ToDateTime(0);

        private HeaderStyle _style;

        public MultitiersDateTimeAxis()
        {
            MinimumRangeTimeSpan = TimeSpan.FromMilliseconds(2);
            HeaderRows = new List<HeaderRow>()
            {
                new HeaderRow
                {
                    ZoomLevels = new List<HeaderZoomLevel>
                    {
                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Milliseconds),
                            Renderers = { new FirstRowRenderer { DateFormat = "MMM dd, HH:mm:ss" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Seconds),
                            Renderers = { new FirstRowRenderer { DateFormat = "MMM dd, HH:mm:ss" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Minutes),
                            Renderers = { new FirstRowRenderer { DateFormat = "MMM dd, HH:mm" } },
                        },

                        new HeaderZoomLevel
                        {
                            Renderers = { new FirstRowRenderer { DateFormat = "MMM dd, htt" } },
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Hours),
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Days),
                            Renderers = { new FirstRowRenderer { DateFormat = "ddd, MMMM d" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Months),
                            Renderers = { new FirstRowRenderer { DateFormat = "MMMM \\'yy" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Years),
                            Renderers = { new FirstRowRenderer { DateFormat = "yyyy" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(10, DateTimeIntervalType.Years),
                            Renderers = { new FirstRowRenderer { DateFormat = "yyyy" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(100, DateTimeIntervalType.Years),
                            Renderers = { new FirstRowRenderer { DateFormat = "yyyy" } },
                        },
                    }
                },

                new HeaderRow
                {
                    ZoomLevels = new List<HeaderZoomLevel>
                    {
                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Milliseconds),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "fff", BottomTextFormat = "\\m\\s" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Seconds),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "ss", BottomTextFormat = "\\sec" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Minutes),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "mm", BottomTextFormat = "\\min" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Hours),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "%h", BottomTextFormat = "tt" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Days),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "%d", BottomTextFormat = "ddd" } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(2, DateTimeIntervalType.Days),
                            IsAccessoryLevel = true,
                            Renderers =
                            {
                                new WeekClearingBackgroundRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                                new SecondRowRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, TopTextFormat = "%d", BottomTextFormat = null, },
                                new SeparatorsRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                            },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(4, DateTimeIntervalType.Days),
                            IsAccessoryLevel = true,
                            Renderers =
                            {
                                new WeekClearingBackgroundRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                                new SecondRowRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, TopTextFormat = "%d", BottomTextFormat = null, },
                                new SeparatorsRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                            },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(7, DateTimeIntervalType.Days),
                            IsAccessoryLevel = true,
                            Renderers =
                            {
                                new WeekClearingBackgroundRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                                new SecondRowRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, TopTextFormat = "%d", BottomTextFormat = null, },
                                new SeparatorsRenderer { OrderIndex = RenderingOrder.WeeksLayerOverrided, },
                            },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Months),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "MMM", BottomTextFormat = null } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(1, DateTimeIntervalType.Years),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "yy", BottomTextFormat = null } },
                        },

                        new HeaderZoomLevel
                        {
                            CellStep = new DateTimeInterval(100, DateTimeIntervalType.Years),
                            Renderers = { new SecondRowRenderer { TopTextFormat = "yyyy", BottomTextFormat = null } },
                        },
                    }
                },
            };

            Style = new HeaderStyle();

            foreach (var row in HeaderRows)
            {
                row.Axis = this;
            }

            SeparatorsRenderer firstRowSeparatorsRenderer = new SeparatorsRenderer
            {
                OrderIndex = RenderingOrder.FirstRowSeparatorLines
            };
            foreach (var zoomLevel in HeaderRows[0].ZoomLevels)
            {
                zoomLevel.Renderers.Add(firstRowSeparatorsRenderer);
            }

            SeparatorsRenderer secondRowSeparatorsRenderer = new SeparatorsRenderer { OrderIndex = 0 };
            WeekendsRenderer weekendRenderer = new WeekendsRenderer();
            foreach (var zoomLevel in HeaderRows[1].ZoomLevels)
            {
                bool isDayGroupLevel = zoomLevel.CellStep.IntervalType == DateTimeIntervalType.Days &&
                                       zoomLevel.CellStep.Count > 1;
                if (!isDayGroupLevel)
                {
                    zoomLevel.Renderers.Insert(0, weekendRenderer);
                }
               
                zoomLevel.Renderers.Add(secondRowSeparatorsRenderer);
            }

            GenerateAccessoryStates();
        }

        public OxyRect HeaderArea { get; private set; }

        public HeaderStyle Style
        {
            get
            {
                return _style;
            }

            set
            {
                if (_style != value)
                {
                    _style = value;
                    OnStyleChanged();
                }
            }
        }

        private void OnStyleChanged()
        {
            if (Style == null)
            {
                return;
            }

            HeaderRows[0].Style = Style.FirstRow;
            HeaderRows[1].Style = Style.SecondRow;
        }

        public List<HeaderRow> HeaderRows { get; private set; }

        public TimeSpan MinimumRangeTimeSpan
        {
            get
            {
                var date = DateTimeAxis.ToDateTime(this.MinimumRange);
                return date - ZeroDate;
            }

            set
            {
                var dateValue = ZeroDate + value;
                this.MinimumRange = DateTimeAxis.ToDouble(dateValue);
            }
        }

        public override OxySize Measure(IRenderContext rc)
        {
            return this.DesiredSize = new OxySize(0, 70);
        }

        private void GenerateAccessoryStates()
        {
            TimeSpan maxIntervalSize = TimeSpan.FromDays(365 * 100);
            int[] usualMultiplier = new int[] { 2, 5, 10 };
            int[] timeMultipliers = new int[] { 2, 5, 10, 30 };
            int[] hoursMultipliers = new int[] { 2, 3, 4, 6, 12 };
            int[] daysMultipliers = new int[] { 40 };
            int[] monthMultipliers = new int[] { 2, 3, 4, 6 };

            foreach (var row in HeaderRows.Skip(1))
            {
                for (int i = 0; i < row.ZoomLevels.Count - 1; i++)
                {
                    var originalZoomLevel = row.ZoomLevels[i];

                    TimeSpan nextIntervalSize = maxIntervalSize;
                    if (i < row.ZoomLevels.Count - 1)
                    {
                        nextIntervalSize = row.ZoomLevels[i + 1].CellStep.MinValue;
                    }

                    int[] multipliers = usualMultiplier;
                    switch (originalZoomLevel.CellStep.IntervalType)
                    {
                        case DateTimeIntervalType.Months:
                            multipliers = monthMultipliers;
                            break;

                        case DateTimeIntervalType.Minutes:
                        case DateTimeIntervalType.Seconds:
                            multipliers = timeMultipliers;
                            break;

                        case DateTimeIntervalType.Hours:
                            multipliers = hoursMultipliers;
                            break;

                        case DateTimeIntervalType.Days:
                            multipliers = daysMultipliers;
                            break;
                    }

                    int multiplierIndex = 0;
                    DateTimeInterval originalInterval = originalZoomLevel.CellStep;
                    while (true)
                    {
                        int multiplier = multipliers[multiplierIndex];
                        DateTimeInterval accessoryInterval = new DateTimeInterval(originalInterval.Count * multiplier, originalInterval.IntervalType);
                        if (accessoryInterval.MaxValue >= nextIntervalSize)
                        {
                            break;
                        }

                        HeaderZoomLevel accessoryLevel = new HeaderZoomLevel
                        {
                            Renderers = new List<RowRenderer>(originalZoomLevel.Renderers),
                            CellStep = accessoryInterval,
                            IsAccessoryLevel = true,
                            MinCellWidth = originalZoomLevel.MinCellWidth,
                        };

                        i++;
                        row.ZoomLevels.Insert(i, accessoryLevel);
                        multiplierIndex++;
                        if (multiplierIndex >= multipliers.Length)
                        {
                            multiplierIndex = 0;
                            originalInterval = accessoryInterval;
                        }
                    }
                }
            }
        }

        public override void Render(IRenderContext rc, int pass)
        {
            if (pass != 0)
            {
                return;
            }

            ArrangeHeader(rc);
            DetermineMinCellLength(rc);

            UpdateHeader();

            
            var renderer = new MultitiersDateTimeAxisRenderer(this, rc, pass);
            renderer.Render();
        }

        private void ArrangeHeader(IRenderContext rc)
        {
            var firstRowTop = this.PlotModel.PlotAndAxisArea.Top;
            
            var totalHeight = this.PlotModel.PlotArea.Top - this.PlotModel.PlotAndAxisArea.Top;

            double firstRowHeight = totalHeight * 0.55;
            var secondRowTop = firstRowTop + firstRowHeight;

            HeaderArea = new OxyRect(this.PlotModel.PlotArea.Left, firstRowTop, this.PlotModel.PlotArea.Width, totalHeight);
            HeaderRows[0].RowArea = new OxyRect(this.PlotModel.PlotArea.Left, firstRowTop, this.PlotModel.PlotArea.Width, firstRowHeight);
            HeaderRows[1].RowArea = new OxyRect(this.PlotModel.PlotArea.Left, secondRowTop, this.PlotModel.PlotArea.Width, totalHeight - firstRowHeight);
        }

        private void DetermineMinCellLength(IRenderContext rc)
        {
            if (_isMinCellWidthDetermined)
            {
                return;
            }

            _isMinCellWidthDetermined = true;
            foreach (var row in HeaderRows)
            {
                foreach (var zoomLevel in row.ZoomLevels)
                {
                    double minWidth = 0;
                    foreach (var renderer in zoomLevel.Renderers)
                    {
                        var rendererCellWidth = renderer.MeasureMinCellWidth(rc);
                        if (rendererCellWidth > minWidth)
                        {
                            minWidth = rendererCellWidth;
                        }
                    }

                    if (minWidth == 0)
                    {
                        minWidth = 1;
                    }

                    zoomLevel.MinCellWidth = minWidth;
                }
            }
        }

        private bool _isMinCellWidthDetermined;

        private double _actualMinimum;

        private double _actualMaximum;


        private void UpdateHeader()
        {
            if (_actualMinimum == this.ActualMinimum &&
                _actualMaximum == this.ActualMaximum)
            {
                return;
            }

            _actualMinimum = this.ActualMinimum;
            _actualMaximum = this.ActualMaximum;

            HeaderRows[1].UpdateActualZoomLevel(null);
            HeaderRows[0].UpdateActualZoomLevel(HeaderRows[1].ActualZoomLevel.CellStep);
            HeaderRows[1].TryApplyNonAccessoryZoomLevel(HeaderRows[0].ActualZoomLevel.CellStep);

            foreach (var row in HeaderRows)
            {
                row.UpdateCells(this.ActualMinimum, this.ActualMaximum);
            }
        }
    }
}
