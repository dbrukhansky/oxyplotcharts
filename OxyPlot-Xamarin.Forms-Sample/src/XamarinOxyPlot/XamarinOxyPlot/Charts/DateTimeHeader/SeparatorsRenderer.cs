﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts.DateTimeHeader
{
    public class SeparatorsRenderer : RowRenderer
    {
        public override void Draw(HeaderRow row, IRenderContext rc)
        {
            var axis = row.Axis;
            foreach (var cell in row.Cells)
            {
                var screenX = axis.Transform(cell.Start);
                var screenTopY = axis.PlotModel.PlotArea.Top;
                var screenBottomY = axis.PlotModel.PlotArea.Bottom;

                bool canDraw = IsChartSeparatorLineVisible(row);
                if (canDraw)
                {
                    rc.DrawLine(screenX, screenTopY, screenX, screenBottomY, row.Style.PlotAreaSeparators);
                }
                
                rc.DrawLine(screenX, row.RowArea.Top, screenX, axis.PlotModel.PlotArea.Top, row.Style.CellSeparators);
            }
        }

        private bool IsChartSeparatorLineVisible(HeaderRow row)
        {
            bool isVisible = true;
            bool isTopRow = row == row.Axis.HeaderRows.First();
            if (isTopRow)
            {
                var secondRowCellInterval = row.Axis.HeaderRows[1].ActualZoomLevel.CellStep;
                if (secondRowCellInterval.IntervalType == OxyPlot.Axes.DateTimeIntervalType.Days &&
                    secondRowCellInterval.Count > 1)
                {
                    // Separators of first and second rows has different locations. Ignore lines of top row
                    isVisible = false;
                }
            }

            return isVisible;
        }
    }
}
