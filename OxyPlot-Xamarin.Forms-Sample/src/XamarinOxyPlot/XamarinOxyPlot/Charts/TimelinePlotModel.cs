﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Charts.DateTimeHeader;
using XamarinOxyPlot.Data;

namespace XamarinOxyPlot.Charts
{
    
    public class TimelinePlotModel : PlotModel
    {
        private ChartData _chartData;

        private ChartStyle _style;

        private MultitiersDateTimeAxis _dateTimeAxis;

        private LineNamesAxis _lineNamesAxis;

        private DownsamplingService _downsamplingEngine = new DownsamplingService();

        private System.Threading.SynchronizationContext _syncContext;

        private ActionDispatcher<IRenderContext> _renderingDispatcher = new ActionDispatcher<IRenderContext>();

        public TimelinePlotModel()
        {
            _syncContext = System.Threading.SynchronizationContext.Current;
            Settings = new DataConfiguration();
            PlotType = PlotType.XY;
            Padding = new OxyThickness(0, 0, 0, 0);
            IsLegendVisible = false;

            _lineNamesAxis = new LineNamesAxis();

            Axes.Add(_lineNamesAxis);

            _dateTimeAxis = new MultitiersDateTimeAxis
            {
                Position = AxisPosition.Top,
                Minimum = 0,
                Maximum = 180,
            };

            Axes.Add(_dateTimeAxis);
            Style = new ChartStyle();
        }

        public ChartData ChartData
        {
            get
            {
                return _chartData;
            }

            set
            {
                if (_chartData != value)
                {
                    _chartData = value;
                    _renderingDispatcher.Enqueue(ConvertDataToSeries, IsChartDataReadyForPreparing);
                    IsReady = false;
                    _syncContext.Post((state) =>
                    {
                        this.InvalidatePlot(true);
                    }, null);
                }
            }
        }

        public bool IsReady { get; set; }

        public DataConfiguration Settings { get; set; }

        public ChartStyle Style
        {
            get
            {
                return _style;
            }

            set
            {
                if (_style != value)
                {
                    _style = value;
                    OnStyleChanged();
                }
            }
        }

        private void OnStyleChanged()
        {
            if (Style == null)
            {
                return;
            }

            _lineNamesAxis.Style = Style.LineNamesAxis;
            _dateTimeAxis.Style = Style.DateTimeHeader;
        }

        protected override void RenderOverride(IRenderContext rc, double width, double height)
        {
            lock (SyncRoot)
            {
                base.RenderOverride(rc, width, height);
                _renderingDispatcher.Execute(rc);
            }
        }

        private bool IsChartDataReadyForPreparing()
        {
            return _chartData != null &&
                   Width > 0 &&
                   Height > 0;
        }

        private void ConvertDataToSeries()
        {
            Task.Run(() =>
            {
                _lineNamesAxis.Arrange(_chartData, Settings);
                var pendedSeries = FillLines();
                GenerateZoomLevelsData(pendedSeries);
                _renderingDispatcher.Enqueue((rc) => DisplayPreparedSeries(pendedSeries, rc), IsChartDataReadyForPreparing);
                _syncContext.Post((state) =>
                {
                    this.InvalidatePlot(true);
                }, null);
            });
        }

        private void DisplayPreparedSeries(List<Series> preparedSeries, IRenderContext rc)
        {
            Series.Clear();
            foreach (var item in preparedSeries)
            {
                Series.Add(item);
            }

            MeasureDataBounds();
            MeasureTextLines(rc);

            _syncContext.Post((state) =>
            {
                this.InvalidatePlot(true);
            }, null);
        }

        private List<Series> FillLines()
        {
            var series = new List<Series>(_chartData.Lines.Count);
            int lineIndex = 0;
            foreach (var line in _chartData.Lines)
            {
                Series convertedSeries = null;
                if (line is PointsLine)
                {
                    convertedSeries = ConvertToLineSeries((PointsLine)line, lineIndex);
                }
                else if (line is RangesLine)
                {
                    convertedSeries = ConvertToRangesSeries((RangesLine)line, lineIndex);
                }

                var chartLine = convertedSeries as IChartDataLine;
                if (chartLine != null)
                {
                    chartLine.LineIndex = lineIndex;
                }

                if (convertedSeries != null)
                {
                    series.Add(convertedSeries);
                }

                lineIndex++;
            }

            return series;
        }

        private void MeasureDataBounds()
        {
            MeasureXBounds();
            _lineNamesAxis.MeasureYBounds(_chartData);
        }

        private void MeasureXBounds()
        {
            double minX = Double.MaxValue;
            double maxX = Double.MinValue;
            foreach (var series in this.Series)
            {
                if (series is PointsSeries)
                {
                    PointsSeries points = (PointsSeries)series;
                    if (points.OriginalPoints.Count > 0)
                    {
                        minX = Math.Min(minX, points.OriginalPoints.First().X);
                        maxX = Math.Max(maxX, points.OriginalPoints.Last().X);
                    }
                }

                if (series is RangesSeries)
                {
                    RangesSeries ranges = (RangesSeries)series;
                    if (ranges.Items.Count > 0)
                    {
                        minX = Math.Min(minX, ranges.Items.First().Start);
                        maxX = Math.Max(maxX, ranges.Items.Last().End);
                    }
                }
            }

            if (maxX == Double.MaxValue || minX == Double.MinValue)
            {
                return;
            }

            double absoluteXLength = maxX - minX;
            double dataMargins = absoluteXLength * 0.1;
            minX -= dataMargins;
            maxX += dataMargins;

            _dateTimeAxis.AbsoluteMinimum = minX;
            _dateTimeAxis.AbsoluteMaximum = maxX;
            _dateTimeAxis.Minimum = _dateTimeAxis.AbsoluteMinimum;
            _dateTimeAxis.Maximum = _dateTimeAxis.AbsoluteMaximum;
            _dateTimeAxis.Reset();
        }

        private void GenerateZoomLevelsData(List<Series> pendingSeries)
        {
            _downsamplingEngine.Configure(this._lineNamesAxis.LineTotalHeight, new OxySize(PlotArea.Width, PlotArea.Height));
            foreach (PointsSeries lineSeries in pendingSeries.Where(l => l is PointsSeries))
            {
                _downsamplingEngine.CreateZoomLayers(lineSeries);
            }
        }

        private void MeasureTextLines(IRenderContext renderContext)
        {
            Stopwatch sw = Stopwatch.StartNew();
            foreach (RangesSeries barSeries in this.Series.Where(l => l is RangesSeries))
            {
                barSeries.MeasureTextItems(renderContext);
            }

            Debug.WriteLine("All text measured: " + sw.ElapsedMilliseconds);
        }

        private PointsSeries ConvertToLineSeries(PointsLine pointsLine, int lineIndex)
        {
            PointsSeries result = new PointsSeries
            {
                LineIndex = lineIndex,
            };

            result.Title = pointsLine.Name;
            foreach (TimelinePoint point in pointsLine.Points)
            {
                double convertedValue = _lineNamesAxis.ConvertToChartScaledValue(point.Value, lineIndex);
                DataPoint convertedPoint = DateTimeAxis.CreateDataPoint(point.DateTime, convertedValue);
                result.OriginalPoints.Add(convertedPoint);
            }

            return result;
        }

        private RangesSeries ConvertToRangesSeries(RangesLine rangesLine, int lineIndex)
        {
            RangesSeries result = new RangesSeries
            {
                BarWidth = _lineNamesAxis.LineHeight / (_lineNamesAxis.LineHeight + _lineNamesAxis.LineSpacing / 2) 
            }; 

            result.Title = rangesLine.Name;
            foreach (var item in rangesLine.Ranges)
            {
                int convertedPosition = (int)_lineNamesAxis.ConvertToChartScaledValue(0, lineIndex);
                IntervalBarItem bar = new RangeBarItem
                {
                    Start = DateTimeAxis.ToDouble(item.Start),
                    End = DateTimeAxis.ToDouble(item.End),
                    CategoryIndex = convertedPosition,
                    Title = item.Text
                };

                result.Items.Add(bar);
            }

            return result;
        }
    }
}
