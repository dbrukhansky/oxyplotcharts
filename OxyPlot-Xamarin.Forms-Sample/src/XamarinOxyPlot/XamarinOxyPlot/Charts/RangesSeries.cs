﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinOxyPlot.Charts
{
    public class RangesSeries : IntervalBarSeries, IChartDataLine
    {
        public int LineIndex { get; set; }

        public void MeasureTextItems(IRenderContext renderContext)
        {
            foreach (RangeBarItem item in this.Items)
            {
                item.PreparedText = renderContext.PrepareText(item.Title,
                                                              ActualFont,
                                                              ActualFontSize,
                                                              ActualFontWeight,
                                                              0,
                                                              HorizontalAlignment.Center,
                                                              VerticalAlignment.Middle);

            }
        }

        protected override void RenderTitle(IRenderContext rc, IntervalBarItem item, OxyRect rectangle)
        {
            RangeBarItem rangeItem = item as RangeBarItem;
            if (rangeItem != null)
            {
                double textPadding = 5;
                var maxTextSize = new OxySize(rectangle.Width - textPadding, rectangle.Height);
                var pt = new ScreenPoint((rectangle.Left + rectangle.Right) / 2, (rectangle.Top + rectangle.Bottom) / 2);
                rc.DrawText(pt, rangeItem.PreparedText, this.ActualTextColor, maxTextSize);
            }
        }

        protected override OxyRect ArrangeRectangleOnScreen(IntervalBarItem item, int defaultCategoryIndex, double actualBarWidth)
        {
            var spaceSize = 1 - this.BarWidth;
            var categoryIndex = item.CategoryIndex;
            double categoryValue = categoryIndex + spaceSize / 2; 

            var p0 = this.Transform(item.Start, categoryValue);
            var p1 = this.Transform(item.End, categoryValue + actualBarWidth);

            var rectangle = OxyRect.Create(p0.X, p0.Y, p1.X, p1.Y);
            return rectangle;
        }

        protected override void UpdateValidData()
        {
            //base.UpdateValidData();
            if (this.Items != null)
            {
                this.ValidItems = this.Items;
            }
        }

        protected override void UpdateMaxMin()
        {
            base.UpdateMaxMin();
            if (Items == null || Items.Count == 0)
            {
                return;
            }

            MinX = this.Items.First().Start;
            MaxX = this.Items.Last().End; 
        }

    }
}
