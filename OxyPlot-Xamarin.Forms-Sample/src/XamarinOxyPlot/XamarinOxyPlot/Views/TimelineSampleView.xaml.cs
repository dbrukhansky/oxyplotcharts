﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinOxyPlot.Views
{
    public partial class TimelineSampleView  : ContentPage
    {
        public TimelineSampleView(object parameter)
        {
            InitializeComponent();
            this.BindingContext = App.Locator.TimelineSampleViewModel;
        }

        public TimelineSampleView()
            : this(null)
        {
            
        }
    }
}