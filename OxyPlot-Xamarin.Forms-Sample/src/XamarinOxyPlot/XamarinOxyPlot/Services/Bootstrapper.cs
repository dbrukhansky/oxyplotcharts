﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XamarinOxyPlot.Data;
using XamarinOxyPlot.Services.Navigation;
using XamarinOxyPlot.ViewModels;
using XamarinOxyPlot.ViewModels.Base;

namespace XamarinOxyPlot.Services
{
    public class Bootstrapper
    {
        private UnityContainer _container;

        public void Run()
        {
            _container = new UnityContainer();
            Initialize(_container);
        }

        public UnityContainer Container
        {
            get
            {
                return _container;
            }
        }

        public virtual void Initialize(UnityContainer container)
        {
            _container.RegisterInstance<UnityContainer>(container);

            // ViewModels
            _container.RegisterType<MainViewModel>();
            _container.RegisterType<TimelineSampleViewModel>();
            _container.RegisterType<ViewModelLocator>();

            // Data
            _container.RegisterType<LocalDataService>();


            // Services     
            _container.RegisterType<INavigationService, NavigationService>();
        }
    }
}
