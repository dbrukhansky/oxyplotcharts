﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using XamarinOxyPlot.Services;
using XamarinOxyPlot.Data;
using XamarinOxyPlot.UWP.Data;

namespace XamarinOxyPlot.UWP.Services
{
    public class PlatformBootstrapper : Bootstrapper
    {
        public override void Initialize(UnityContainer container)
        {
            base.Initialize(container);
            container.RegisterType<ILocalResourcesProvider, LocalResourcesProvider>();
        }
    }
}
