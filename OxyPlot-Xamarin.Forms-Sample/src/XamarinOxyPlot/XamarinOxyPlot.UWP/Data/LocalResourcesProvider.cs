﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Storage;
using XamarinOxyPlot.Data;

namespace XamarinOxyPlot.UWP.Data
{
    public class LocalResourcesProvider : ILocalResourcesProvider
    {
        public async Task<IEnumerable<string>> FindAsync(string path)
        {
            path = path ?? string.Empty;
            path = path.Replace("/", "\\");
            StorageFolder folder = Package.Current.InstalledLocation;
            path = Path.Combine("Assets", path);

            folder = await Package.Current.InstalledLocation.GetFolderAsync(path);

            var files = await folder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.DefaultQuery);
            var filePath = files.Select(f => f.Path).ToArray();
            return filePath;
        }

        public async Task<Stream> OpenAsync(string resourceName)
        {
            StorageFile file = await StorageFile.GetFileFromPathAsync(resourceName);
            Stream stream = await file.OpenStreamForReadAsync();
            return stream;
        }
    }
}
