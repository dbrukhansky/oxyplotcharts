﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OxyPlot
{
    public static class ScreenPointsReduction
    {
        public static IList<ScreenPoint> DPReductionVariant(IList<ScreenPoint> Points, int nPoints)
        {
            //Check some preconditions
            if (Points == null || Points.Count < 3 || Points.Count <= nPoints)
                return Points;

            //init two lists which are containing the polynoms 
            var pointsToReturn = new List<ScreenPoint>();
            var polynoms = new List<Polynom>();

            //Create and add the first polynom from the original list of points
            polynoms.Add(new Polynom(Points));

            //split the polynoms as long as the expected number of points is not reached
            for (int i = 0; i < nPoints - 2; i++)
            {
                Polynom p = polynoms.OrderByDescending(x => x.MaxDistance).First();
                polynoms.Insert(polynoms.IndexOf(p), p.SplitAtMaxDistance());
            }

            //convert the edges of the polynoms back to a list of points
            pointsToReturn.Add(polynoms[0].FirstPoint);
            pointsToReturn.AddRange(polynoms.Select(polynom => polynom.LastPoint));

            return pointsToReturn;
        }

        private class Polynom
        {
            public ScreenPoint FirstPoint { get { return polynompoints.First(); } }
            public ScreenPoint LastPoint { get { return polynompoints.Last(); } }
            private readonly List<ScreenPoint> polynompoints = new List<ScreenPoint>();
            private int MaxDistancePointIndex { get; set; }
            public double MaxDistance { get; private set; }

            public Polynom(IEnumerable<ScreenPoint> PolynomPoints)
            {
                polynompoints.AddRange(PolynomPoints);
                CalcDistance();
            }

            /// <summary>
            /// calculate the point of the maximum distance within THIS polynom
            /// </summary>
            private void CalcDistance()
            {
                MaxDistance = 0;
                for (int i = 0; i < polynompoints.Count - 1; i++)
                {
                    double currdist = PerpendicularDistance(FirstPoint, LastPoint, polynompoints[i]);
                    if (currdist > MaxDistance)
                    {
                        MaxDistancePointIndex = i;
                        MaxDistance = currdist;
                    }
                }
            }

            /// <summary>
            /// Split THIS polynom at the maximum distance index into two polynoms
            /// </summary>
            public Polynom SplitAtMaxDistance()
            {
                List<ScreenPoint> pl = polynompoints.GetRange(0, MaxDistancePointIndex + 1);
                polynompoints.RemoveRange(0, MaxDistancePointIndex);
                CalcDistance();
                return new Polynom(pl);
            }
        }

        /// <summary>
        /// Calculate the perpendicular distance of a given point
        /// </summary>        
        private static Double PerpendicularDistance(ScreenPoint firstPoint, ScreenPoint lastPoint, ScreenPoint currentPoint)
        {
            Double area = Math.Abs(.5 * (firstPoint.X * lastPoint.Y + lastPoint.X * currentPoint.Y + currentPoint.X * firstPoint.Y - lastPoint.X * firstPoint.Y - currentPoint.X * lastPoint.Y - firstPoint.X * currentPoint.Y));
            Double bottom = Math.Sqrt(Math.Pow(firstPoint.X - lastPoint.X, 2) + Math.Pow(firstPoint.Y - lastPoint.Y, 2));
            Double height = area / bottom * 2;
            return height;
        }
    }
}
