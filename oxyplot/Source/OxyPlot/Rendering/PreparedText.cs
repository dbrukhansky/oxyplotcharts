﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyPlot
{
    public class PreparedText
    {
        public string Text { get; set; }

        public string FontFamily { get; set; }

        public double FontSize { get; set; }

        public double FontWeight { get; set; }

        public double Rotation { get; set; }

        public HorizontalAlignment HorizontalAlignment { get; set; }

        public VerticalAlignment VerticalAlignment { get; set; }

        public OxySize? MaxSize { get; set; }

        public OxySize TextSize { get; set; } 

        public TrimmingMode Trimming { get; set; }

        public OxySize MinTextSize { get; set; }

        public object NativeState { get; set; }
    }

    public enum TrimmingMode
    {
        None,
        Clip,
        Word,
        Character,
        CharacterEllipsis,
        WordEllipsis,
    }
}
