﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyPlot
{
    public class TextStyle
    {
        public TextStyle()
        {
            FontFamily = null;
            FontSize = 10;
            FontWeight = FontWeights.Normal;
            Rotation = 0;
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
            TextTrimming = TrimmingMode.CharacterEllipsis;

        }

        public OxyColor TextColor { get; set; }

        public string FontFamily { get; set; }
        
        public double FontWeight { get; set; }

        public double FontSize { get; set; }

        public double Rotation { get; set; }

        public VerticalAlignment VerticalAlignment { get; set; }

        public HorizontalAlignment HorizontalAlignment { get; set; }

        public TrimmingMode TextTrimming { get; set; }
    }
}
