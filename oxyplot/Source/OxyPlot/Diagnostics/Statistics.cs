﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OxyPlot.Diagnostics
{
    public class Statistics
    {
        public static Statistics Instance = new Statistics();

        public int PointsCount { get; set; }

        public int RectanglesCount { get; set; }

        public int ClippingInvokes { get; set; }

        public long DrawingMilliseconds { get; set; }

        public long PreparingMillisecods { get; set; }
    }

    public class FpsService
    {
        private FpsMeasurement currentInstance;

        private FpsMeasurement preparedInstance;

        public double Fps
        {
            get
            {
                Initialize();
                return this.currentInstance.Fps;
            }
        }

        private void Initialize()
        {
            if (this.currentInstance == null)
            {
                this.currentInstance = new FpsMeasurement();
            }

            if (preparedInstance == null)
            {
                this.preparedInstance = new FpsMeasurement();
            }

            if (this.preparedInstance.IsReady)
            {
                this.currentInstance = this.preparedInstance;
                this.preparedInstance = new FpsMeasurement();
            }
        }

        public void Reset()
        {
            this.currentInstance = null;
            this.preparedInstance = null;
            Initialize();
        }


        public void NotifyAboutNewFrame()
        {
            Initialize();
            this.currentInstance.FramesCount++;
            this.preparedInstance.FramesCount++;
        }
    }

    public class FpsMeasurement
    {
        private Stopwatch stopwatch;

        public int FramesCount { get; set; }

        public double Fps
        {
            get
            {
                return FramesCount / stopwatch.Elapsed.TotalSeconds;
            }
        }

        public FpsMeasurement()
        {
            stopwatch = Stopwatch.StartNew();
        }

        public bool IsReady
        {
            get
            {
                return stopwatch.Elapsed.TotalSeconds > 1;
            }
        }

        private void StopMeasurements()
        {
            stopwatch.Stop();
        }
    }
}
