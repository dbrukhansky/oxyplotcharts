﻿using SkiaSharp;
using SkiaSharp.Views.UWP;
using System;
using Windows.ApplicationModel;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;


namespace OxyPlot.Windows
{
    public partial class SKXamlCanvas : Canvas
    {
        private static bool designMode = DesignMode.DesignModeEnabled;

        private IntPtr pixels;
        private WriteableBitmap bitmap;
        private double dpi;
        private bool ignorePixelScaling;

        public SKXamlCanvas()
        {
            Initialize();
        }

        private void Initialize()
        {
            if (designMode)
                return;

            SizeChanged += OnSizeChanged;
            Unloaded += OnUnloaded;

            // get the scale from the current display
            var display = DisplayInformation.GetForCurrentView();
            OnDpiChanged(display);
            display.DpiChanged += OnDpiChanged;
        }

        public SKSize CanvasSize => bitmap == null ? SKSize.Empty : new SKSize(bitmap.PixelWidth, bitmap.PixelHeight);

        public bool IgnorePixelScaling
        {
            get { return ignorePixelScaling; }
            set
            {
                ignorePixelScaling = value;
                Invalidate();
            }
        }

        public double Dpi => dpi;

        private void OnDpiChanged(DisplayInformation sender, object args = null)
        {
            dpi = sender.LogicalDpi / 96.0f;
            Invalidate();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Invalidate();
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            FreeBitmap();
        }

        public void Invalidate()
        {
            isRedrawRequired = true;
            var action = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, DoInvalidate);
        }

        private SKSurface currentSurface;

        public Action<SKSurface> RedrawAction { get; set; }

        public void Redraw()
        {
            Invalidate();
        }

        public void Redraw1()
        {
            if (designMode)
                return;

            if (ActualWidth == 0 || ActualHeight == 0 ||
                double.IsNaN(ActualWidth) || double.IsNaN(ActualHeight) ||
                double.IsInfinity(ActualWidth) || double.IsInfinity(ActualHeight) ||
                Visibility != Visibility.Visible)
                return;

            int width, height;
            if (IgnorePixelScaling)
            {
                width = (int)ActualWidth;
                height = (int)ActualHeight;
            }
            else
            {
                width = (int)(ActualWidth * dpi);
                height = (int)(ActualHeight * dpi);
            }

            var info = new SKImageInfo(width, height, SKImageInfo.PlatformColorType, SKAlphaType.Premul);
            CreateBitmap(info);
            using (var surface = SKSurface.Create(info, pixels, info.RowBytes))
            {
                RedrawAction?.Invoke(surface);
            }

            bitmap.Invalidate();
        }

        private bool isRedrawRequired;

        private bool isRedrawPended;

        private bool isRedrawExecuted;

        private async void SetupRedraw()
        {
            isRedrawPended = true;

            if (isRedrawExecuted)
            {
                return;
            }

            isRedrawExecuted = true;
            try
            {
                while (isRedrawPended)
                {
                    isRedrawPended = false;
                    await System.Threading.Tasks.Task.Delay(10);
                    Redraw1();
                }
            }
            finally
            {
                isRedrawExecuted = false;
            }
        }

        private async void DoInvalidate()
        {
            //if (!isRedrawRequired)
            //{
            //    return;
            //}

            //isRedrawRequired = false;
            SetupRedraw();


            //try
            //{
            //    await System.Threading.Tasks.Task.Delay(50);
            //    if (!isRedrawRequired)
            //    {
            //        return;
            //    }

            //    Redraw1();
            //}
            //finally
            //{
            //    isRedrawRequired = false;
            //}
            //if (designMode)
            //    return;

            //if (ActualWidth == 0 || ActualHeight == 0 ||
            //    double.IsNaN(ActualWidth) || double.IsNaN(ActualHeight) ||
            //    double.IsInfinity(ActualWidth) || double.IsInfinity(ActualHeight) ||
            //    Visibility != Visibility.Visible)
            //    return;

            //int width, height;
            //if (IgnorePixelScaling)
            //{
            //    width = (int)ActualWidth;
            //    height = (int)ActualHeight;
            //}
            //else
            //{
            //    width = (int)(ActualWidth * dpi);
            //    height = (int)(ActualHeight * dpi);
            //}

            //var info = new SKImageInfo(width, height, SKImageInfo.PlatformColorType, SKAlphaType.Premul);
            //CreateBitmap(info);
            //using (var surface = SKSurface.Create(info, pixels, info.RowBytes))
            //{
            //    OnPaintSurface(new SKPaintSurfaceEventArgs(surface, info));
            //}
            //bitmap.Invalidate();
        }

        private void CreateBitmap(SKImageInfo info)
        {
            if (bitmap == null || bitmap.PixelWidth != info.Width || bitmap.PixelHeight != info.Height)
            {
                FreeBitmap();

                bitmap = new WriteableBitmap(info.Width, info.Height);
                pixels = bitmap.GetPixels();

                var brush = new ImageBrush
                {
                    ImageSource = bitmap,
                    AlignmentX = AlignmentX.Left,
                    AlignmentY = AlignmentY.Top,
                    Stretch = Stretch.None
                };
                if (!IgnorePixelScaling)
                {
                    var scale = 1.0 / dpi;
                    brush.Transform = new ScaleTransform
                    {
                        ScaleX = scale,
                        ScaleY = scale
                    };
                }
                Background = brush;
            }
        }

        private void FreeBitmap()
        {
            Background = null;
            bitmap = null;
            pixels = IntPtr.Zero;
        }
    }
}
