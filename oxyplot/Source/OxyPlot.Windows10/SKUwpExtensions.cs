﻿using System;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

using System.Reflection;
using System.Linq;
using SkiaSharp.Views.UWP;

namespace OxyPlot.Windows
{
    public static class UWPExtensions1
    {
        public static IntPtr GetPixels(this WriteableBitmap bitmap)
        {
            var pixelsMethod = typeof(UWPExtensions).GetRuntimeMethods().FirstOrDefault(m => m.Name == "GetPixels");
            var res = (IntPtr)pixelsMethod.Invoke(null, new object[] { bitmap });
            return res;



            //var buffer = bitmap.PixelBuffer as IBufferByteAccess;
            //if (buffer == null)
            //    throw new InvalidCastException("Unable to convert WriteableBitmap.PixelBuffer to IBufferByteAccess.");

            //IntPtr ptr;
            //var hr = buffer.Buffer(out ptr);
            //if (hr < 0)
            //    throw new InvalidCastException("Unable to retrieve pixel address from WriteableBitmap.PixelBuffer.");

            //return ptr;
        }
    }
}
