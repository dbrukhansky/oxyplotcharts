﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.Text;
using Microsoft.Graphics.Canvas.UI.Xaml;
using OxyPlot.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.UI.Text;

namespace OxyPlot.Windows
{
    public class Win2dRenderContext : RenderContextBase
    {
        private CanvasControl swapChainPanel;

        private CanvasSwapChain swapChain;

        private CanvasDrawingSession drawingSession;

        private OxySize panelSize;

        public Win2dRenderContext(CanvasControl panel, CanvasDrawingSession drawingSession)
        {
            this.panelSize = new OxySize(panel.ActualWidth, panel.ActualHeight);
            this.swapChainPanel = panel;
            this.drawingSession = drawingSession;
        }

        /// <summary>
        /// Converts the specified rectangle to a scaled rectangle.
        /// </summary>
        /// <param name="rect">The rectangle to convert.</param>
        /// <returns>The converted rectangle.</returns>
        private Rect Convert(OxyRect rect)
        {
            return new Rect(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        public override void DrawLine(IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            var pathGeometry =  this.SetPath(points, aliased);
            var style = new CanvasStrokeStyle
            {
                LineJoin = lineJoin.ToWindows(),
            };

            if (dashArray != null && dashArray.Length > 0)
            {
                style.CustomDashStyle = dashArray.Select(i => (float)i).ToArray();
            }


            drawingSession.DrawGeometry(pathGeometry, stroke.ToWindows(), (float)thickness, style);

            var milliseconds1 = sw.ElapsedMilliseconds; sw.Stop();

            Statistics.Instance.PointsCount += points.Count;
            Statistics.Instance.PreparingMillisecods += milliseconds1;

        }

        private CanvasGeometry SetPath(IList<ScreenPoint> points, bool aliased, bool isClosed = false)
        {
            CanvasPathBuilder pathBuilder = new CanvasPathBuilder(swapChainPanel);
            if (aliased)
            {
                pathBuilder.BeginFigure((float)points[0].X, (float)points[0].Y);
                for (int i = 1; i < points.Count; i++)
                {
                    var x = (float)points[i].X;
                    var y = (float)points[i].Y;
                
                    //if (i < 2)
                    {
                        pathBuilder.AddLine(x, y);
                    }
                }
            }
            else
            {
                pathBuilder.BeginFigure((float)points[0].X, (float)points[0].Y);
                for (int i = 1; i < points.Count; i++)
                {
                    var x = (float)points[i].X;
                    var y = (float)points[i].Y;
                    //if (i < 2)
                    {
                        pathBuilder.AddLine(x, y);
                    }
                }
            }

            CanvasFigureLoop figureLoop = CanvasFigureLoop.Open;
            if (isClosed)
            {
                figureLoop = CanvasFigureLoop.Closed;
            }
        
            pathBuilder.EndFigure(figureLoop);
            var geometry = CanvasGeometry.CreatePath(pathBuilder);
            return geometry;
        }

        public override void DrawRectangle(OxyRect rect, OxyColor fill, OxyColor stroke, double thickness)
        {
            Statistics.Instance.RectanglesCount++;
            Rect drawingRect = new Rect(rect.Left, rect.Top, rect.Width, rect.Height);
            if (fill.IsVisible())
            {
                this.drawingSession.FillRectangle(drawingRect, fill.ToWindows());
            }

            if (stroke.IsVisible())
            {
                this.drawingSession.DrawRectangle(drawingRect, stroke.ToWindows(), (float)thickness);
            }

        }

        private CanvasActiveLayer clippingLayer;

        public override bool SetClip(OxyRect rect)
        {
            Statistics.Instance.ClippingInvokes++;;
            ResetClip();
            clippingLayer = this.drawingSession.CreateLayer(1, Convert(rect));
            return clippingLayer != null;
        }

        public override void ResetClip()
        {
            if (clippingLayer == null)
            {
                return;
            }
            
            clippingLayer.Dispose();
            clippingLayer = null;
        }

        public override void DrawPolygon(IList<ScreenPoint> points, OxyColor fill, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            var figure = this.SetPath(points, aliased, true);
            if (fill.IsVisible())
            {
                this.drawingSession.FillGeometry(figure, fill.ToWindows());
            }

            if (stroke.IsVisible())
            {
                this.drawingSession.DrawGeometry(figure, stroke.ToWindows(), (float)thickness);
            }
        }

        public override void DrawText(ScreenPoint p, string text, OxyColor fill, string fontFamily, double fontSize, double fontWeight, double rotate, HorizontalAlignment halign, VerticalAlignment valign, TrimmingMode textTrimming, OxySize? maxSize)
        {
            CanvasTextLayout textLayout = null;
            text = text ?? string.Empty;

            bool isCached = textCache.TryGetValue(text, out textLayout);
            if (!isCached)
            {
                textLayout = this.CreateTextLayout(text, fontFamily, fontSize, fontWeight, maxSize);
                textCache[text] = textLayout;
            }

            Rect textLayoutRect = textLayout.DrawBounds;


            var dx = halign == HorizontalAlignment.Left ? 0d : (halign == HorizontalAlignment.Center ? -textLayoutRect.Width * 0.5 : -textLayoutRect.Width);
            var dy = valign == VerticalAlignment.Top ? 0d : (valign == VerticalAlignment.Middle ? -textLayoutRect.Height * 0.5 : -textLayoutRect.Height);
            var x0 = -textLayoutRect.Left;
            var y0 = -textLayoutRect.Top;

            //var originalTransform = drawingSession.Transform;
            //drawingSession.Transform = System.Numerics.Matrix3x2.CreateTranslation((float)p.X, (float)p.Y) * System.Numerics.Matrix3x2.CreateRotation((float)rotate);
            //drawingSession.Transform *= System.Numerics.Matrix3x2.CreateTranslation((float)(dx + x0), (float)(dy + y0));
            
            drawingSession.DrawTextLayout(textLayout, (float)p.X + (float)(dx + x0), (float)p.Y + (float)(dy + y0), fill.ToWindows());

            //drawingSession.DrawTextLayout(textLayout, 0f, 0f, fill.ToWindows());


            //var format = new CanvasTextFormat
            //{
            //    FontSize = (float)fontSize,
            //    FontWeight = new FontWeight { Weight = (ushort)fontWeight },
            //    WordWrapping = CanvasWordWrapping.NoWrap,

            //};
            //if (fontFamily != null)
            //{
            //    format.FontFamily = fontFamily;
            //}
            //drawingSession.DrawText(text, 0f, 0f, fill.ToWindows(), format);


            //drawingSession.Transform = originalTransform;
        }

        private static Dictionary<string, CanvasTextLayout> textCache = new Dictionary<string, CanvasTextLayout>();


        private CanvasTextLayout CreateTextLayout(string text, string fontFamily, double fontSize, double fontWeight, OxySize? maxSize)
        {
            CanvasTextFormat format = new CanvasTextFormat
            {
                FontSize = (float)fontSize,
                FontWeight = new FontWeight { Weight = (ushort)fontWeight },
                WordWrapping = CanvasWordWrapping.NoWrap,
            };

            if (fontFamily != null)
            {
                format.FontFamily = fontFamily;
            }

            text = text ?? string.Empty;

            float maxWidth = 0.0f;
            float maxHeight = 0.0f;

            if (maxSize.HasValue)
            {
                maxWidth = (float)maxSize.Value.Width;
                maxHeight = (float)maxSize.Value.Height;
            }

            CanvasTextLayout textLayout = new CanvasTextLayout(this.swapChainPanel, text, format, maxWidth, maxHeight);

            return textLayout;
        }

        private static CanvasTextLayout cachedTextLayout;

        public override OxySize MeasureText(string text, string fontFamily, double fontSize, double fontWeight)
        {
            CanvasTextLayout textLayout = null;
            text = text ?? string.Empty;

            bool isCached = textCache.TryGetValue(text, out textLayout);
            if (!isCached)
            {
                textLayout = this.CreateTextLayout(text, fontFamily, fontSize, fontWeight, null);
                textCache[text] = textLayout;
            }


            //CanvasTextFormat format = new CanvasTextFormat
            //{
            //    FontSize = (float)fontSize,
            //    FontWeight = new FontWeight { Weight = (ushort)fontWeight },
            //    WordWrapping = CanvasWordWrapping.NoWrap,
            //};

            //if (fontFamily != null)
            //{
            //    format.FontFamily = fontFamily;
            //}

            //text = text ?? string.Empty;

            //CanvasTextLayout textLayout = new CanvasTextLayout(this.drawingSession, text, format, 0.0f, 0.0f);

            return new OxySize(textLayout.DrawBounds.Width, textLayout.DrawBounds.Height);
        }

        public override void CleanUp()
        {
            base.CleanUp();
            ResetClip();
        }
    }
}
