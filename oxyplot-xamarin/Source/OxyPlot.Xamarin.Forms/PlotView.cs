﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlotView.cs" company="OxyPlot">
//   Copyright (c) 2014 OxyPlot contributors
// </copyright>
// <summary>
//   Represents a visual element that displays a PlotModel.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace OxyPlot.Xamarin.Forms
{
	using System;

	using OxyPlot;

	using global::Xamarin.Forms;

	public class PlotViewInternal : ContentView
	{
		/// <summary>
		/// Identifies the <see cref="Controller" />  bindable property.
		/// </summary>
		public static readonly BindableProperty ControllerProperty = BindableProperty.Create(nameof(Controller), typeof(PlotController), typeof(PlotViewInternal));

		/// <summary>
		/// Identifies the <see cref="Model" />  bindable property.
		/// </summary>
		public static readonly BindableProperty ModelProperty = BindableProperty.Create(nameof(Model), typeof(PlotModel), typeof(PlotViewInternal));

		/// <summary>
		/// Initializes a new instance of the <see cref="PlotView"/> class.
		/// </summary>
		/// <exception cref="InvalidOperationException">Renderer is not initialized</exception>
		public PlotViewInternal()
		{
			if (!IsRendererInitialized)
			{
				var message = "Renderer is not initialized.";
				switch (Device.OS)
				{
					case TargetPlatform.Windows:
						message +=
							"\nRemember to add `OxyPlot.Xamarin.Forms.Platform.UWP.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init(e);` in the Universal Windows app project.";
						break;
					case TargetPlatform.WinPhone:
						message +=
							"\nRemember to add `OxyPlot.Xamarin.Forms.Platform.WP8.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the Windows Phone app project.";
						break;
					case TargetPlatform.Android:
						message +=
							"\nRemember to add `OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the Android app project.";
						break;
					case TargetPlatform.iOS:
						message +=
							"\nRemember to add `OxyPlot.Xamarin.Forms.Platform.iOS.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the iOS app project.";
						break;
				}
				throw new InvalidOperationException(message);
			}
		}

		public PlotView Owner { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the renderer is "initialized".
		/// </summary>
		/// <value><c>true</c> if the renderer is initialized; otherwise, <c>false</c>.</value>
		public static bool IsRendererInitialized { get; set; }

		/// <summary>
		/// Gets or sets the <see cref="PlotModel"/> to view.
		/// </summary>
		/// <value>The model.</value>
		public PlotModel Model
		{
			get { return (PlotModel)this.GetValue(ModelProperty); }
			set { this.SetValue(ModelProperty, value); }
		}

		/// <summary>
		/// Gets or sets the <see cref="PlotController"/> for the view.
		/// </summary>
		/// <value>The controller.</value>
		public PlotController Controller
		{
			get { return (PlotController)this.GetValue(ControllerProperty); }
			set { this.SetValue(ControllerProperty, value); }
		}
	}
    

    /// <summary>
    /// Represents a visual element that displays a <see cref="PlotModel" />.
    /// </summary>
    public class PlotView : ContentView
    {
        /// <summary>
        /// Identifies the <see cref="Controller" />  bindable property.
        /// </summary>
        public static readonly BindableProperty ControllerProperty = BindableProperty.Create(nameof(Controller), typeof(PlotController), typeof(PlotView));

        /// <summary>
        /// Identifies the <see cref="Model" />  bindable property.
        /// </summary>
        public static readonly BindableProperty ModelProperty = BindableProperty.Create(nameof(Model), typeof(PlotModel), typeof(PlotView));

        /// <summary>
        /// Initializes a new instance of the <see cref="PlotView"/> class.
        /// </summary>
        /// <exception cref="InvalidOperationException">Renderer is not initialized</exception>
        public PlotView()
        {
            if (!IsRendererInitialized)
            {
                var message = "Renderer is not initialized.";
                switch (Device.OS)
                {
                    case TargetPlatform.Windows:
                        message +=
                            "\nRemember to add `OxyPlot.Xamarin.Forms.Platform.UWP.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init(e);` in the Universal Windows app project.";
                        break;
                    case TargetPlatform.WinPhone:
                        message +=
                            "\nRemember to add `OxyPlot.Xamarin.Forms.Platform.WP8.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the Windows Phone app project.";
                        break;
                    case TargetPlatform.Android:
                        message +=
                            "\nRemember to add `OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the Android app project.";
                        break;
                    case TargetPlatform.iOS:
                        message +=
                            "\nRemember to add `OxyPlot.Xamarin.Forms.Platform.iOS.PlotViewRenderer.Init();` after `Xamarin.Forms.Forms.Init();` in the iOS app project.";
                        break;
                }
                throw new InvalidOperationException(message);
            }

			CreateContent();
        }

        /// <summary>
        /// Gets or sets the <see cref="PlotModel"/> to view.
        /// </summary>
        /// <value>The model.</value>
        public PlotModel Model
        {
            get { return (PlotModel)this.GetValue(ModelProperty); }
            set { this.SetValue(ModelProperty, value); }
        }

        /// <summary>
        /// Gets or sets the <see cref="PlotController"/> for the view.
        /// </summary>
        /// <value>The controller.</value>
        public PlotController Controller
        {
            get { return (PlotController)this.GetValue(ControllerProperty); }
            set { this.SetValue(ControllerProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the renderer is "initialized".
        /// </summary>
        /// <value><c>true</c> if the renderer is initialized; otherwise, <c>false</c>.</value>
        public static bool IsRendererInitialized { get; set; }

        public bool IsDiagnosticEnabled
        {
            get
            {
                return _statisticsPanel.IsVisible;
            }

            set
            {
                _statisticsPanel.IsVisible = value;
            }
        }

        public void CreateContent()
        {
            _statisticsPanel = new Grid
            {
				BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.Start,
                HorizontalOptions = LayoutOptions.Start,
                WidthRequest = 130,
                IsVisible = false,
            };

            _statisticsLabel = new Label()
            {
                TextColor = Color.Green,
            };

            _statisticsPanel.Children.Add(_statisticsLabel);

			if (Device.RuntimePlatform == Device.iOS)
			{


				var internalControl = new PlotViewInternal
				{
					Owner = this
				};

				var grid = new Grid
				{
					Children =
					{
						internalControl,
						_statisticsPanel,
					}
				};

				PropertyChanged += (s, e) =>
				{
					if (e.PropertyName == PlotView.ControllerProperty.PropertyName)
					{
						internalControl.Controller = Controller;
					}
					else if (e.PropertyName == PlotView.ModelProperty.PropertyName)
					{
						internalControl.Model = Model;
					}
					else if (e.PropertyName == PlotView.IsVisibleProperty.PropertyName)
					{
						internalControl.IsVisible = IsVisible;
					}
					else if (e.PropertyName == PlotView.BackgroundColorProperty.PropertyName)
					{
						internalControl.BackgroundColor = BackgroundColor;
					}
				};

				Content = grid;
			}
			else
			{
				Content = _statisticsPanel;
			}
        }

        private Label _statisticsLabel;

        private Grid _statisticsPanel;

        public void DisplayDiagnosticInfo(string text)
        {
            _statisticsLabel.Text = text;
        }
    }
}
