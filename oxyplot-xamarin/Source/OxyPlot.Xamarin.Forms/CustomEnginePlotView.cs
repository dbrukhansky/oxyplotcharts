﻿using System;

using OxyPlot;

using global::Xamarin.Forms;

namespace OxyPlot.Xamarin.Forms
{
    public enum DrawingEngine
    {
        SkiaSharpGl,
        SkiaSharp,
        Win2D,
    }

    public class CustomEnginePlotView : PlotView
    {
        /// <summary>
        /// Identifies the <see cref="Controller" />  bindable property.
        /// </summary>
        public static readonly BindableProperty DrawingEngineProperty = BindableProperty.Create(nameof(DrawingEngine), typeof(DrawingEngine), typeof(PlotView), DrawingEngine.SkiaSharpGl);

        public DrawingEngine DrawingEngine
        {
            get { return (DrawingEngine)this.GetValue(DrawingEngineProperty); }
            set { this.SetValue(DrawingEngineProperty, value); }
        }
    }
}
