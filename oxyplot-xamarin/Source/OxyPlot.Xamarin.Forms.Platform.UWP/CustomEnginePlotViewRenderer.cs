﻿using OxyPlot.Xamarin.Forms;
using OxyPlot.Xamarin.Forms.Platform.UWP;

using global::Xamarin.Forms;
using global::Xamarin.Forms.Platform.UWP;

// Exports the renderer.
[assembly: ExportRenderer(typeof(CustomEnginePlotView), typeof(CustomEnginePlotViewRenderer))]


namespace OxyPlot.Xamarin.Forms.Platform.UWP
{
    using System.ComponentModel;

    using OxyPlot.Windows;
    using global::Windows.UI.Xaml;
    using global::Windows.UI.Xaml.Controls;

    /// <summary>
    /// Provides a custom <see cref="OxyPlot.Xamarin.Forms.PlotView" /> renderer for UWP projects with xamarin forms. 
    /// </summary>
    public class CustomEnginePlotViewRenderer : ViewRenderer<Xamarin.Forms.CustomEnginePlotView, FrameworkElement>
    //    public class PlotViewRenderer : ViewRenderer<Xamarin.Forms.PlotView, Win2dPlotView>
    {
        /// <summary>
        /// Initializes static members of the <see cref="CustomEnginePlotViewRenderer"/> class.
        /// </summary>
        static CustomEnginePlotViewRenderer()
        {
            Init();
        }

        private FrameworkElement nativeControl;

        private ContentControl container;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomEnginePlotViewRenderer"/> class.
        /// </summary>
        public CustomEnginePlotViewRenderer()
        {
            // Do not delete
        }

        /// <summary>
        /// Initializes the renderer.
        /// </summary>
        /// <remarks>This method must be called before a <see cref="T:PlotView" /> is used.</remarks>
        public static void Init()
        {
            OxyPlot.Xamarin.Forms.PlotView.IsRendererInitialized = true;
        }

        /// <summary>
        /// Raises the element changed event.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.CustomEnginePlotView> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || this.Element == null)
            {
                return;
            }

            


            //var plotView = new Win2dPlotView
            container = new ContentControl
            {
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch,
            };

            this.SetNativeControl(container);
            SetupPlotViewControl();
        }

        private void SetupPlotViewControl()
        {
            this.nativeControl = null;
            var model = (IPlotModel)this.Element.Model;
            if (model != null)
            {
                model.DetachPlotView();
            }
            
            switch (this.Element.DrawingEngine)
            {
                case DrawingEngine.SkiaSharp:
                    this.nativeControl = new SKPlotView
                    {
                        Model = this.Element.Model,
                        Controller = this.Element.Controller,
                        Background = this.Element.BackgroundColor.ToOxyColor().ToBrush()
                    };
                    break;

                case DrawingEngine.SkiaSharpGl:
                    var skGlPlotView = new SKPlotViewGL
                    {
                        Model = this.Element.Model,
                        Controller = this.Element.Controller,
                        Background = this.Element.BackgroundColor.ToOxyColor().ToBrush()
                    };

                    skGlPlotView.DiagnosticInfoUpdated += OnDiagnosticInfoUpdated;
                    this.nativeControl = skGlPlotView;
                    break;

                case DrawingEngine.Win2D:
                    var win2dControl = new Win2dPlotView
                    {
                        Model = this.Element.Model,
                        Controller = this.Element.Controller,
                        Background = this.Element.BackgroundColor.ToOxyColor().ToBrush()
                    };
                    win2dControl.DiagnosticInfoUpdated += OnDiagnosticInfoUpdated;

                    this.nativeControl = win2dControl;
                    break;
            }

            container.Content = this.nativeControl;
        }

        private void OnDiagnosticInfoUpdated(object sender, string diagnosticInfo)
        {
            this.Element.DisplayDiagnosticInfo(diagnosticInfo);
        }

        /// <summary>
        /// Raises the element property changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (this.Element == null || this.Control == null)
            {
                return;
            }

            if (e.PropertyName == Xamarin.Forms.CustomEnginePlotView.DrawingEngineProperty.PropertyName)
            {
                this.SetupPlotViewControl();
            }

            if (e.PropertyName == Xamarin.Forms.PlotView.ModelProperty.PropertyName)
            {
               ((dynamic) this.nativeControl).Model = this.Element.Model;
            }

            if (e.PropertyName == Xamarin.Forms.PlotView.ControllerProperty.PropertyName)
            {
                ((dynamic)this.nativeControl).Controller = this.Element.Controller;
            }

            if (e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName)
            {
                ((dynamic)this.nativeControl).Background = this.Element.BackgroundColor.ToOxyColor().ToBrush();
            }
        }
    }
}
