﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace OxyPlot.Xamarin.Shared
{
    public class PreparedTextState
    {
        private TextMeasurments _textMeasurements;

        private TextMeasurments _minTextMeasurements;

        public PreparedTextState()
        {
            //TrimmedTextStates = new Dictionary<string, TextMeasurments>();
        }

        public OxySize TextSize
        {
            get
            {
                return _textMeasurements.TextSize;
            }
        }

        public OxySize MinTextSize
        {
            get
            {
                OxySize minSize = new OxySize();
                if (_minTextMeasurements != null)
                {
                    minSize = _minTextMeasurements.TextSize;
                }

                return minSize;
            }
        }

        public float ScaledFontSize { get; set; }

        public TextMeasurments TextMeasurements
        {
            get
            {
                return _textMeasurements;
            }

            set
            {
                _textMeasurements = value;
                //TrimmedTextStates[_textMeasurements.Text] = value;
            }
        }

        public TextMeasurments MinTextMeasurements
        {
            get
            {
                return _minTextMeasurements;
            }

            set
            {
                _minTextMeasurements = value;
                //TrimmedTextStates[_minTextMeasurements.Text] = value;
            }
        }

        //public Dictionary<string, TextMeasurments> TrimmedTextStates { get; set; }
    }

    public class TextMeasurments
    {
        public string Text { get; set; }

        public OxySize TextSize { get; set; }

        public float AlignmentTranslationX { get; set; }

        public float AlignmentTranslationY { get; set; }

    }
}
