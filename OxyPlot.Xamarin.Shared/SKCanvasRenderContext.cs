﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CanvasRenderContext.cs" company="OxyPlot">
//   Copyright (c) 2014 OxyPlot contributors
// </copyright>
// <summary>
//   Provides a render context for Android.Graphics.Canvas.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace OxyPlot.Xamarin.Shared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SkiaSharp;
    using System.Threading.Tasks;
    using OxyPlot.Diagnostics;
    using System.Diagnostics;

#if __IOS__
    using iOS;

#elif __ANDROID__
    using OxyPlot.Xamarin.Android;

#endif

#if WINDOWS_UWP
    using Windows;
#endif

    /// <summary>
    /// Provides a render context for Android.Graphics.Canvas.
    /// </summary>
    public class SKCanvasRenderContext : RenderContextBase
    {
        /// <summary>
        /// The images in use
        /// </summary>
        private readonly HashSet<OxyImage> imagesInUse = new HashSet<OxyImage>();

        /// <summary>
        /// The image cache
        /// </summary>
        private readonly Dictionary<OxyImage, SKBitmap> imageCache = new Dictionary<OxyImage, SKBitmap>();

        private PaintsStorage _paintStorage = new PaintsStorage();

        /// <summary>
        /// A reusable path object.
        /// </summary>
        private readonly SKPath path;

        /// <summary>
        /// A reusable bounds rectangle.
        /// </summary>
        private readonly SKRect bounds;

        /// <summary>
        /// A reusable list of points.
        /// </summary>
        private readonly List<float> pts;

        /// <summary>
        /// The canvas to draw on.
        /// </summary>
        private SKCanvas canvas;

        /// <summary>
        /// Initializes a new instance of the <see cref="SKCanvasRenderContext" /> class.
        /// </summary>
        /// <param name="scale">The scale.</param>
        public SKCanvasRenderContext(double scale)
        {
            this.path = new SKPath();
            this.bounds = new SKRect();
            this.pts = new List<float>();
            this.Scale = scale;
        }

        /// <summary>
        /// Gets the factor that this.Scales from OxyPlot´s device independent pixels (96 dpi) to 
        /// Android´s density-independent pixels (160 dpi).
        /// </summary>
        /// <remarks>See <a href="http://developer.android.com/guide/practices/screens_support.html">Supporting multiple screens.</a>.</remarks>
        public double Scale { get; private set; }

        /// <summary>
        /// Sets the target.
        /// </summary>
        /// <param name="c">The canvas.</param>
        public void SetTarget(SKCanvas c)
        {
            this.canvas = c;
        }

        /// <summary>
        /// Draws an ellipse.
        /// </summary>
        /// <param name="rect">The rectangle.</param>
        /// <param name="fill">The fill color.</param>
        /// <param name="stroke">The stroke color.</param>
        /// <param name="thickness">The thickness.</param>
        public override void DrawEllipse(OxyRect rect, OxyColor fill, OxyColor stroke, double thickness)
        {
            if (fill.IsVisible())
            {
                var fillPaint = _paintStorage.GetPaint(PaintKind.Fill);
                fillPaint.SetFill(fill);
                this.canvas.DrawOval(this.Convert(rect), fillPaint);
            }

            if (stroke.IsVisible())
            {
                var strokePaint = _paintStorage.GetPaint(PaintKind.Stroke);
                strokePaint.SetStroke(stroke, thickness, Scale);
                this.canvas.DrawOval(this.Convert(rect), strokePaint);
            }
        }

        /// <summary>
        /// Draws the collection of ellipses, where all have the same stroke and fill.
        /// This performs better than calling DrawEllipse multiple times.
        /// </summary>
        /// <param name="rectangles">The rectangles.</param>
        /// <param name="fill">The fill color.</param>
        /// <param name="stroke">The stroke color.</param>
        /// <param name="thickness">The stroke thickness.</param>
        public override void DrawEllipses(IList<OxyRect> rectangles, OxyColor fill, OxyColor stroke, double thickness)
        {
            if (rectangles.Count == 0)
            {
                return;
            }

            if (fill.IsVisible())
            {
                var fillPaint = _paintStorage.GetPaint(PaintKind.Fill);
                fillPaint.SetFill(fill);
                foreach (var rect in rectangles)
                {
                    this.canvas.DrawOval(this.Convert(rect), fillPaint);
                }
            }

            if (stroke.IsVisible())
            {
                var strokePaint = _paintStorage.GetPaint(PaintKind.Stroke);
                strokePaint.SetStroke(stroke, thickness, Scale);
                foreach (var rect in rectangles)
                {
                    this.canvas.DrawOval(this.Convert(rect), strokePaint);
                }
            }
        }

        /// <summary>
        /// Draws a polyline.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="stroke">The stroke color.</param>
        /// <param name="thickness">The stroke thickness.</param>
        /// <param name="dashArray">The dash array.</param>
        /// <param name="lineJoin">The line join type.</param>
        /// <param name="aliased">if set to <c>true</c> the shape will be aliased.</param>
        public override void DrawLine(IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            var scale = this.Scale;
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            var linePaint = _paintStorage.GetPaint(PaintKind.Line);
            linePaint.SetStroke(stroke, thickness, Scale, dashArray, lineJoin, aliased);
            linePaint.StrokeWidth = 1;

            var _points = new SKPoint[points.Count];

            Parallel.For(0, points.Count, i =>
            {
                _points[i] = new SKPoint((float)(points[i].X * scale), (float)(points[i].Y * scale));
            });

            this.canvas.DrawPoints(SKPointMode.Polygon, _points, linePaint);

            var drawingMilliseconds = sw.ElapsedMilliseconds; sw.Stop();
            Statistics.Instance.PointsCount += points.Count;
            Statistics.Instance.PreparingMillisecods += drawingMilliseconds;
		}

        public override void DrawLine(IList<DataPoint> points, OxyColor stroke, int minPointIndex, int maxPointIndex, double xOffset, double yOffset, 
                                      double xScale, double yScale, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
        {
            xScale *= this.Scale;
            yScale *= this.Scale;

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            var linePaint = _paintStorage.GetPaint(PaintKind.Line);
            linePaint.SetStroke(stroke, thickness, Scale, dashArray, lineJoin, aliased);
            linePaint.StrokeWidth = 1;

            // include the point[maxPointIndex] to visible points.
            maxPointIndex++;

            int pointsCount = maxPointIndex - minPointIndex;

            var _points = new SKPoint[pointsCount];
            Parallel.For(minPointIndex, maxPointIndex, i =>
            {
                var x = (points[i].X - xOffset) * xScale;
                var y = (points[i].Y - yOffset) * yScale;
                _points[i - minPointIndex] = new SKPoint((float)x, (float)y);
            });

            this.canvas.DrawPoints(SKPointMode.Polygon, _points, linePaint);

            var drawingMilliseconds = sw.ElapsedMilliseconds; sw.Stop();
            Statistics.Instance.PointsCount += points.Count;
            Statistics.Instance.PreparingMillisecods += drawingMilliseconds;
        }

		/// <summary>
		/// Draws multiple line segments defined by points (0,1) (2,3) (4,5) etc.
		/// This should have better performance than calling DrawLine for each segment.
		/// </summary>
		/// <param name="points">The points.</param>
		/// <param name="stroke">The stroke color.</param>
		/// <param name="thickness">The stroke thickness.</param>
		/// <param name="dashArray">The dash array.</param>
		/// <param name="lineJoin">The line join type.</param>
		/// <param name="aliased">If set to <c>true</c> the shape will be aliased.</param>
		public override void DrawLineSegments(IList<ScreenPoint> points, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
		{
            var linePaint = _paintStorage.GetPaint(PaintKind.Line);
            linePaint.SetStroke(stroke, thickness, Scale, dashArray, lineJoin, aliased);

			if (aliased)
			{
				for (int i = 0; i < points.Count; i += 2)
				{
					var pt0 = points[i];
					var pt1 = points[i + 1];

					this.canvas.DrawLine(this.ConvertAliased(pt0.X), this.ConvertAliased(pt0.Y), this.ConvertAliased(pt1.X), this.ConvertAliased(pt1.Y), linePaint);
				}
			}
			else
			{
				for (int i = 0; i < points.Count; i += 2)
				{
					var pt0 = points[i];
					var pt1 = points[i + 1];

					this.canvas.DrawLine(this.Convert(pt0.X), this.Convert(pt0.Y), this.Convert(pt1.X), this.Convert(pt1.Y), linePaint);
				}
			}
		}

		/// <summary>
		/// Draws a polygon. The polygon can have stroke and/or fill.
		/// </summary>
		/// <param name="points">The points.</param>
		/// <param name="fill">The fill color.</param>
		/// <param name="stroke">The stroke color.</param>
		/// <param name="thickness">The stroke thickness.</param>
		/// <param name="dashArray">The dash array.</param>
		/// <param name="lineJoin">The line join type.</param>
		/// <param name="aliased">If set to <c>true</c> the shape will be aliased.</param>
		public override void DrawPolygon(IList<ScreenPoint> points, OxyColor fill, OxyColor stroke, double thickness, double[] dashArray, LineJoin lineJoin, bool aliased)
		{
			this.path.Reset();

		    this.SetPath(points, aliased);
			this.path.Close();


            
			if (fill.IsVisible())
			{
                var fillPaint = _paintStorage.GetPaint(PaintKind.Fill);
                fillPaint.SetFill(fill);
				this.canvas.DrawPath(this.path, fillPaint);
			}

			if (stroke.IsVisible())
			{
                var strokePaint = _paintStorage.GetPaint(PaintKind.Stroke);
                strokePaint.SetStroke(stroke, thickness, Scale, dashArray, lineJoin, aliased);
				this.canvas.DrawPath(this.path, strokePaint);
			}
		}

        /// <summary>
        /// Draws a rectangle.
        /// </summary>
        /// <param name="rect">The rectangle.</param>
        /// <param name="fill">The fill color.</param>
        /// <param name="stroke">The stroke color.</param>
        /// <param name="thickness">The stroke thickness.</param>
        public override void DrawRectangle(OxyRect rect, OxyColor fill, OxyColor stroke, double thickness)
		{
            Statistics.Instance.RectanglesCount++;
            
            {
                if (fill.IsVisible())
                {
                    var fillPaint = _paintStorage.GetPaint(PaintKind.Fill);
                    fillPaint.SetFill(fill);
                    this.canvas.DrawRect(new SKRect(this.ConvertAliased(rect.Left), this.ConvertAliased(rect.Top), this.ConvertAliased(rect.Right), this.ConvertAliased(rect.Bottom)), fillPaint);
                }

                if (stroke.IsVisible())
                {
                    var strokePaint = _paintStorage.GetPaint(PaintKind.Stroke);
                    strokePaint.SetStroke(stroke, thickness, Scale, aliased: true);
                    this.canvas.DrawRect(new SKRect(this.ConvertAliased(rect.Left), this.ConvertAliased(rect.Top), this.ConvertAliased(rect.Right), this.ConvertAliased(rect.Bottom)), strokePaint);
                }
            }
		}

        /// <summary>
        /// Draws the text.
        /// </summary>
        /// <param name="p">The position of the text.</param>
        /// <param name="text">The text.</param>
        /// <param name="fill">The fill color.</param>
        /// <param name="fontFamily">The font family.</param>
        /// <param name="fontSize">Size of the font.</param>
        /// <param name="fontWeight">The font weight.</param>
        /// <param name="rotate">The rotation angle.</param>
        /// <param name="halign">The horizontal alignment.</param>
        /// <param name="valign">The vertical alignment.</param>
        /// <param name="maxSize">The maximum size of the text.</param>
        public override void DrawText(ScreenPoint p, string text, OxyColor fill, string fontFamily, double fontSize, double fontWeight, double rotate, HorizontalAlignment halign, VerticalAlignment valign, TrimmingMode textTrimming, OxySize? maxSize)
        {
            var preparedText = PrepareText(text,
                                          fontFamily,
                                          fontSize,
                                          fontWeight,
                                          rotate,
                                          halign,
                                          valign,
                                          trimming: textTrimming);

            DrawText(p, preparedText, fill, maxSize);
            return;

//            this.Reset();
//            {
//                this.paint.TextSize = this.Convert(fontSize);
//                this.SetFill(fill);

//                float width;
//                float height;
//                float lineHeight, delta;
//                this.GetFontMetrics(this.paint, out lineHeight, out delta);
//                if (maxSize.HasValue || halign != HorizontalAlignment.Left || valign != VerticalAlignment.Bottom)
//                {
//                    width = this.paint.MeasureText(text);
//                    height = lineHeight;
//                }
//                else
//                {
//                    width = height = 0f;
//                }

//                if (maxSize.HasValue)
//                {
//                    var maxWidth = this.Convert(maxSize.Value.Width);
//                    var maxHeight = this.Convert(maxSize.Value.Height);

//                    if (width > maxWidth)
//                    {
//                        width = maxWidth;
//                    }

//                    if (height > maxHeight)
//                    {
//                        height = maxHeight;
//                    }
//                }

//                var dx = halign == HorizontalAlignment.Left ? 0d : (halign == HorizontalAlignment.Center ? -width * 0.5 : -width);
//                var dy = valign == VerticalAlignment.Bottom ? 0d : (valign == VerticalAlignment.Middle ? height * 0.5 : height);
//                var x0 = -this.bounds.Left;
//                var y0 = delta;

//                this.canvas.Save();
//                this.canvas.Translate(this.Convert(p.X), this.Convert(p.Y));
//                this.canvas.RotateDegrees((float)rotate);
//                this.canvas.Translate((float)dx + x0, (float)dy + y0);

//                if (maxSize.HasValue)
//                {
//                    var x1 = -x0;
//                    var y1 = -height - y0;
//#if !__IOS__
//                    // has problem on gl on ios
//                    this.canvas.ClipRect(new SKRect(x1, y1, x1 + width, y1 + height));
//#endif
//                    this.canvas.Translate(0, lineHeight - height);
//                }

//                this.paint.LcdRenderText = false;
//                this.paint.IsAntialias = false;

//                this.canvas.DrawText(text, 0, 0, this.paint);

//                this.canvas.Restore();
//            }
        }

        /// <summary>
        /// Measures the text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="fontFamily">The font family.</param>
        /// <param name="fontSize">Size of the font.</param>
        /// <param name="fontWeight">The font weight.</param>
        /// <returns>The text size.</returns>
        public override OxySize MeasureText(string text, string fontFamily, double fontSize, double fontWeight)
		{
			if (string.IsNullOrEmpty(text))
			{
				return OxySize.Empty;
			}

            //this.paint.IsAntialias = true;
            var textPaint = _paintStorage.GetPaint(PaintKind.Text);
            textPaint.TextSize = this.Convert(fontSize);
			float lineHeight, delta;
			this.GetFontMetrics(textPaint, out lineHeight, out delta);
			var textWidth = textPaint.MeasureText(text);
			return new OxySize(textWidth / this.Scale, lineHeight / this.Scale);
		}

        public override PreparedText PrepareText(string text,
                                                 string fontFamily,
                                                 double fontSize,
                                                 double fontWeight,
                                                 double rotation,
                                                 HorizontalAlignment horizontalAlignment,
                                                 VerticalAlignment verticalAlignment,
                                                 TrimmingMode trimming)
        {
            text = text ?? string.Empty;
            PreparedText preparedText = base.PrepareText(text,
                                                         fontFamily,
                                                         fontSize,
                                                         fontWeight,
                                                         rotation,
                                                         horizontalAlignment,
                                                         verticalAlignment,
                                                         trimming);


            PreparedTextState textState = new PreparedTextState();
            preparedText.NativeState = textState;

            //TODO: use fontFamily, fontWeight.
            var textPaint = _paintStorage.GetPaint(PaintKind.Text);
            textPaint.TextSize = this.Convert(fontSize);
            textState.ScaledFontSize = textPaint.TextSize;

            DetermineDesiredAndMinTextSizes(textPaint, preparedText);
            OxySize textSize = textState.TextMeasurements.TextSize;

            preparedText.TextSize = new OxySize(textSize.Width / this.Scale, textSize.Height / this.Scale);
            if (textState.MinTextMeasurements != null)
            {
                OxySize minTextSize = textState.MinTextMeasurements.TextSize;
                preparedText.MinTextSize = new OxySize(minTextSize.Width / this.Scale, minTextSize.Height / this.Scale);
            }

            return preparedText;
        }

        private TextMeasurments PrepareTextMeasurements(SKPaint paint, 
                                                        string text, 
                                                        HorizontalAlignment horizontalAlignment, 
                                                        VerticalAlignment verticalAlignment)
        {
            
            float width;
            float height;
            float lineHeight, delta;
            this.GetFontMetrics(paint, out lineHeight, out delta);

            width = paint.MeasureText(text);
            height = lineHeight;

            var dx = horizontalAlignment == HorizontalAlignment.Left ? 0d : (horizontalAlignment == HorizontalAlignment.Center ? -width * 0.5 : -width);
            var dy = verticalAlignment == VerticalAlignment.Bottom ? 0d : (verticalAlignment == VerticalAlignment.Middle ? height * 0.5 : height);
            var x0 = -this.bounds.Left;
            var y0 = delta;

            TextMeasurments state = new TextMeasurments
            {
                Text = text,
                AlignmentTranslationX = (float)dx + x0,
                AlignmentTranslationY = (float)dy + y0,
                TextSize = new OxySize(width, height),
            };

            return state;
        }

        private void DetermineDesiredAndMinTextSizes(SKPaint paint, PreparedText preparedText)
        {
            string text = preparedText.Text;
            TrimmingMode trimming = preparedText.Trimming;
            PreparedTextState textState = (PreparedTextState)preparedText.NativeState; 
            var desiredTextMeasurements = PrepareTextMeasurements(paint, text, preparedText.HorizontalAlignment, preparedText.VerticalAlignment);
            textState.TextMeasurements = desiredTextMeasurements;

            if (trimming == TrimmingMode.None)
            {
                textState.MinTextMeasurements = desiredTextMeasurements;
                return;
            }

            if (trimming == TrimmingMode.Clip)
            {
                return;
            }

            string minText = string.Empty;
            switch (trimming)
            {
                case TrimmingMode.Character:
                case TrimmingMode.CharacterEllipsis:
                    if (text.Length > 1)
                    {
                        minText = text[0].ToString();
                        if (trimming == TrimmingMode.CharacterEllipsis)
                        {
                            minText += EllipsesSymbol;
                        }
                    }
                    else
                    {
                        minText = text;
                    }
                    break;

                case TrimmingMode.Word:
                case TrimmingMode.WordEllipsis:
                    if (text.Length > 1)
                    {
                        int firstWordLength = -1;
                        bool isSearchingForWordStart = true;
                        for (int i = 0; i < text.Length; i++)
                        {
                            char symbol = text[i];
                            if (isSearchingForWordStart)
                            {
                                if (!Char.IsWhiteSpace(symbol))
                                {
                                    isSearchingForWordStart = false;
                                }
                            }
                            else
                            {
                                if (Char.IsWhiteSpace(symbol))
                                {
                                    firstWordLength = i;
                                    break;
                                }
                            }
                        }

                        minText = text;
                        if (firstWordLength > 0 && firstWordLength < text.Length)
                        {
                            minText = text.Substring(0, firstWordLength);
                            if (trimming == TrimmingMode.CharacterEllipsis)
                            {
                                minText += EllipsesSymbol;
                            }
                        }
                    }
                    else
                    {
                        minText = text;
                    }

                    break;

                default:
                    throw new NotSupportedException(String.Format("Renderer doesn't support trimming mode {0}" + trimming));

            }

            textState.MinTextMeasurements = PrepareTextMeasurements(paint,
                                                                    minText,
                                                                    preparedText.HorizontalAlignment,
                                                                    preparedText.VerticalAlignment);

        }

        public override void DrawText(ScreenPoint p, PreparedText text, OxyColor fill, OxySize? maxSize)
        {
            PreparedTextState textState = text.NativeState as PreparedTextState;
            if (textState == null)
            {
                base.DrawText(p, text, fill, maxSize);
                return;
            }

            // Don;t draw anything if text size is too large.
            if (maxSize.HasValue)
            {
                if (maxSize.Value.Width < textState.MinTextSize.Width)
                {
                    return;
                }
            }

            //TODO: use fontFamily, fontWeight.
            var textPaint = _paintStorage.GetPaint(PaintKind.Text);
            textPaint.TextSize = textState.ScaledFontSize;
            textPaint.SetFill(fill);


            string displayedText = text.Text;
            TextMeasurments textForDrawing = DetermineTextForDrawing(textPaint, text, maxSize);
            if (textForDrawing == null)
            {
                return;
            }

            this.canvas.Save();
            this.canvas.Translate(this.Convert(p.X), this.Convert(p.Y));
            this.canvas.RotateDegrees((float)text.Rotation);
            this.canvas.Translate(textForDrawing.AlignmentTranslationX, textForDrawing.AlignmentTranslationY);

            if (maxSize.HasValue)
            {
                if (text.Trimming == TrimmingMode.Clip)
                {
                    float lineHeight, delta;
                    this.GetFontMetrics(textPaint, out lineHeight, out delta);

                    var maxWidth = Convert(maxSize.Value.Width);
                    var maxHeight = Convert(maxSize.Value.Height);

                    float width = (float)textForDrawing.TextSize.Width;
                    float height = (float)textForDrawing.TextSize.Height;
                    if (maxWidth < width)
                    {
                        width = maxWidth;
                    }

                    if (maxHeight < height)
                    {
                        height = maxHeight;
                    }

                    var x0 = -this.bounds.Left;
                    var y0 = delta;

                    var x1 = -x0;
                    var y1 = -height - y0;

#if !__IOS__
                    // has problem on gl on ios
                    this.canvas.ClipRect(new SKRect(x1, y1, x1 + width, y1 + height));
#endif
                    this.canvas.Translate(0, lineHeight - height);
                }
            }

            textPaint.LcdRenderText = false;
            textPaint.IsAntialias = false;
            this.canvas.DrawText(textForDrawing.Text, 0, 0, textPaint);
            this.canvas.Restore();
        }

        private TextMeasurments DetermineTextForDrawing(SKPaint paint, PreparedText preparedText, OxySize? maxSize)
        {
            PreparedTextState textState = preparedText.NativeState as PreparedTextState;
            TextMeasurments textForDrawing = textState.TextMeasurements;

            float maxWidth = float.MaxValue;
            if (maxSize.HasValue)
            {
                maxWidth = Convert(maxSize.Value.Width);
            }

            if (preparedText.Trimming == TrimmingMode.Clip && maxSize.HasValue)
            {
                var width = Math.Min(maxWidth, textForDrawing.TextSize.Width);
                var height = Math.Min(Convert(maxSize.Value.Height), textForDrawing.TextSize.Height);
                var dx = preparedText.HorizontalAlignment == HorizontalAlignment.Left ? 0d : (preparedText.HorizontalAlignment == HorizontalAlignment.Center ? -width * 0.5 : -width);
                var dy = preparedText.VerticalAlignment == VerticalAlignment.Bottom ? 0d : (preparedText.VerticalAlignment == VerticalAlignment.Middle ? height * 0.5 : height);
                var x0 = -this.bounds.Left;
                float lineHeight, delta;
                this.GetFontMetrics(paint, out lineHeight, out delta);
                var y0 = delta;

                TextMeasurments state = new TextMeasurments
                {
                    Text = textForDrawing.Text,
                    AlignmentTranslationX = (float)dx + x0,
                    AlignmentTranslationY = (float)dy + y0,
                    TextSize = new OxySize(width, height),
                };

                return state;
            }

            if (!maxSize.HasValue)
            {
                return textForDrawing;
            }

            
            bool textShouldBeTrimmed = textState.TextSize.Width > maxWidth;
            if (!textShouldBeTrimmed)
            {
                return textForDrawing;
            }

            if (textState.MinTextSize.Width > maxWidth)
            {
                // Available size is too small. Can't draw anything. 
                textForDrawing = null;
                return textForDrawing;
            }

            int trimmedTextLength = (int)paint.BreakText(textState.TextMeasurements.Text, maxWidth);
            if (trimmedTextLength <=  0)
            {
                textForDrawing = null;
                return textForDrawing;
            }

            while (trimmedTextLength > 0)
            {
                string trimmedText = DetermineTrimmedText(preparedText.Text, trimmedTextLength, preparedText.Trimming);
                TextMeasurments trimmedMeasurements = null;
                //bool measuredPreviously = textState.TrimmedTextStates.TryGetValue(trimmedText, out trimmedMeasurements);
                //if (!measuredPreviously)
                //{
                    trimmedMeasurements = PrepareTextMeasurements(paint,
                                                                  trimmedText,
                                                                  preparedText.HorizontalAlignment,
                                                                  preparedText.VerticalAlignment);

                    //textState.TrimmedTextStates[trimmedText] = trimmedMeasurements;
                //}

                if (trimmedMeasurements.TextSize.Width < maxWidth)
                {
                    textForDrawing = trimmedMeasurements;
                    return textForDrawing;
                }

                trimmedTextLength--;
            }

            return textState.MinTextMeasurements;
        }

        private const string EllipsesSymbol = "...";

        private string DetermineTrimmedText(string text, int maxLength, TrimmingMode trimmingMode)
        {
            string resultText = text;
            bool shouldAddEllipses = false;
            if (trimmingMode == TrimmingMode.CharacterEllipsis)
            {
                shouldAddEllipses = true;
                maxLength--;
                trimmingMode = TrimmingMode.Character;
            }

            if (trimmingMode == TrimmingMode.WordEllipsis)
            {
                shouldAddEllipses = true;
                maxLength--;
                trimmingMode = TrimmingMode.Word;
            }

            switch (trimmingMode)
            {
                case TrimmingMode.Character:
                    resultText = text.Substring(0, maxLength);
                    break;

                case TrimmingMode.Word:
                    int lastSymbolInWord = -1;
                    bool isSearchingForWord = true;
                    for (int i = 0; i < maxLength; i++)
                    {
                        char symbol = text[i];
                        if (isSearchingForWord)
                        {
                            if (!Char.IsWhiteSpace(symbol))
                            {
                                isSearchingForWord = false;
                            }
                        }
                        else
                        {
                            if (Char.IsWhiteSpace(symbol))
                            {
                                lastSymbolInWord = i - 1;
                                isSearchingForWord = true;
                            }
                        }
                    }

                    if (lastSymbolInWord >= 0)
                    {
                        resultText = text.Substring(0, lastSymbolInWord + 1);
                    }
                    else
                    {
                        resultText = string.Empty;
                    }

                    break;

                default:
                    throw new NotSupportedException(String.Format("Renderer doesn't support trimming mode {0}" + trimmingMode));

            }

            if (shouldAddEllipses)
            {
                if (resultText.Length > 0)
                {
                    resultText += EllipsesSymbol;
                }
            }

            return resultText;
        } 


        /// <summary>
        /// Sets the clip rectangle.
        /// </summary>
        /// <param name="rect">The clip rectangle.</param>
        /// <returns>True if the clip rectangle was set.</returns>
        public override bool SetClip(OxyRect rect)
		{
			this.canvas.Save();
			this.canvas.ClipRect(this.Convert(rect));
			return true;
		}

		/// <summary>
		/// Resets the clip rectangle.
		/// </summary>
		public override void ResetClip()
		{
			this.canvas.Restore();
		}

		/// <summary>
		/// Draws the specified portion of the specified <see cref="OxyImage" /> at the specified location and with the specified size.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw.</param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw.</param>
		/// <param name="srcWidth">Width of the portion of the source image to draw.</param>
		/// <param name="srcHeight">Height of the portion of the source image to draw.</param>
		/// <param name="destX">The x-coordinate of the upper-left corner of drawn image.</param>
		/// <param name="destY">The y-coordinate of the upper-left corner of drawn image.</param>
		/// <param name="destWidth">The width of the drawn image.</param>
		/// <param name="destHeight">The height of the drawn image.</param>
		/// <param name="opacity">The opacity.</param>
		/// <param name="interpolate">interpolate if set to <c>true</c>.</param>
		public override void DrawImage(
			OxyImage source,
			double srcX,
			double srcY,
			double srcWidth,
			double srcHeight,
			double destX,
			double destY,
			double destWidth,
			double destHeight,
			double opacity,
			bool interpolate)
		{
			var image = this.GetImage(source);
			if (image == null)
			{
				return;
			}

			var src = new SKRect((int)srcX, (int)srcY, (int)(srcX + srcWidth), (int)(srcY + srcHeight));
			var dest = new SKRect(this.Convert(destX), this.Convert(destY), this.Convert(destX + destWidth), this.Convert(destY + destHeight));

            var imagePaint = _paintStorage.GetPaint(PaintKind.Image);

			// TODO: support opacity
			this.canvas.DrawBitmap(image, src, dest, imagePaint);
		}

		/// <summary>
		/// Cleans up resources not in use.
		/// </summary>
		/// <remarks>This method is called at the end of each rendering.</remarks>
		public override void CleanUp()
		{
			var imagesToRelease = this.imageCache.Keys.Where(i => !this.imagesInUse.Contains(i)).ToList();
			foreach (var i in imagesToRelease)
			{
				var image = this.GetImage(i);
				image.Dispose();
				this.imageCache.Remove(i);
			}

			this.imagesInUse.Clear();
		}

		/// <summary>
		/// Gets font metrics for the font in the specified paint.
		/// </summary>
		/// <param name="paint">The paint.</param>
		/// <param name="defaultLineHeight">Default line height.</param>
		/// <param name="delta">The vertical delta.</param>
		private void GetFontMetrics(SKPaint paint, out float defaultLineHeight, out float delta)
		{
			var metrics = paint.FontMetrics;
			var ascent = -metrics.Ascent;
			var descent = metrics.Descent;
			var leading = metrics.Leading;

			//// http://stackoverflow.com/questions/5511830/how-does-line-spacing-work-in-core-text-and-why-is-it-different-from-nslayoutm

			leading = leading < 0 ? 0 : (float)Math.Floor(leading + 0.5f);
			var lineHeight = (float)Math.Floor(ascent + 0.5f) + (float)Math.Floor(descent + 0.5) + leading;
			var ascenderDelta = leading >= 0 ? 0 : (float)Math.Floor((0.2 * lineHeight) + 0.5);
			defaultLineHeight = lineHeight + ascenderDelta;
			delta = ascenderDelta - descent;
		}

		/// <summary>
		/// Converts the specified coordinate to a scaled coordinate.
		/// </summary>
		/// <param name="x">The coordinate to convert.</param>
		/// <returns>The converted coordinate.</returns>
		private float Convert(double x)
		{
			return (float)(x * this.Scale);
		}

		/// <summary>
		/// Converts the specified rectangle to a scaled rectangle.
		/// </summary>
		/// <param name="rect">The rectangle to convert.</param>
		/// <returns>The converted rectangle.</returns>
		private SKRect Convert(OxyRect rect)
		{
			return new SKRect(this.ConvertAliased(rect.Left), this.ConvertAliased(rect.Top), this.ConvertAliased(rect.Right), this.ConvertAliased(rect.Bottom));
		}

		/// <summary>
		/// Converts the specified coordinate to a pixel-aligned scaled coordinate.
		/// </summary>
		/// <param name="x">The coordinate to convert.</param>
		/// <returns>The converted coordinate.</returns>
		private float ConvertAliased(double x)
		{
			return (float)(x * this.Scale) + 0.5f;
		}

		/// <summary>
		/// Sets the path to the specified points.
		/// </summary>
		/// <param name="points">The points defining the path.</param>
		/// <param name="aliased">If set to <c>true</c> aliased.</param>
		private void SetPath(IList<ScreenPoint> points, bool aliased)
		{
			if (aliased)
			{
				this.path.MoveTo(this.ConvertAliased(points[0].X), this.ConvertAliased(points[0].Y));
				for (int i = 1; i < points.Count; i++)
				{
					this.path.LineTo(this.ConvertAliased(points[i].X), this.ConvertAliased(points[i].Y));
				}
			}
			else
			{
				this.path.MoveTo(this.Convert(points[0].X), this.Convert(points[0].Y));
				for (int i = 1; i < points.Count; i++)
				{
					this.path.LineTo(this.Convert(points[i].X), this.Convert(points[i].Y));
				}
			}
		}

		/// <summary>
		/// Gets the image from cache or creates a new <see cref="Bitmap" />.
		/// </summary>
		/// <param name="source">The source image.</param>
		/// <returns>A <see cref="Bitmap" />.</returns>
		private SKBitmap GetImage(OxyImage source)
		{
			if (source == null)
			{
				return null;
			}

			if (!this.imagesInUse.Contains(source))
			{
				this.imagesInUse.Add(source);
			}

			SKBitmap bitmap;
			if (!this.imageCache.TryGetValue(source, out bitmap))
			{
				var bytes = source.GetData();
				bitmap = SKBitmap.Decode(bytes);
				this.imageCache.Add(source, bitmap);
			}

			return bitmap;
		}
    }
}