﻿using System;
using System.Collections.Generic;
using System.Linq;
using SkiaSharp;
using System.Threading.Tasks;
using OxyPlot.Diagnostics;
using System.Diagnostics;


#if __IOS__
    using OxyPlot.Xamarin.iOS;
#elif __ANDROID__
using OxyPlot.Xamarin.Android;

#endif

#if WINDOWS_UWP
    using OxyPlot.Windows;
#endif

namespace OxyPlot.Xamarin.Shared
{
    public static class PaintExtensions
    {
        public static void Reset(this SKPaint paint)
        {
            paint.Color = new SKColor(0, 0, 0);
            paint.Style = SKPaintStyle.Fill;
            paint.StrokeCap = SKStrokeCap.Butt;
            paint.StrokeJoin = SKStrokeJoin.Miter;
            paint.TextAlign = SKTextAlign.Left;
            paint.Typeface = null;
            paint.StrokeWidth = 1.0f;
            paint.StrokeMiter = 4.0f;
            paint.TextSize = 20.0f;
            paint.TextScaleX = 1.0f;
            paint.TextSkewX = 0.0f;
            paint.BlendMode = SKBlendMode.SrcOver;
            paint.ColorFilter = null;
            paint.Shader = null;
            paint.PathEffect = null;
            paint.MaskFilter = null;
            paint.HintingLevel = SKPaintHinting.Normal;
            paint.IsAntialias = false;
        }

        /// <summary>
		/// Sets the fill style.
		/// </summary>
		/// <param name="fill">The fill color.</param>
		public static void SetFill(this SKPaint paint, OxyColor fill)
        {
            paint.Style = SKPaintStyle.Fill;
            paint.Color = fill.ToSKColor();
        }

        public static void SetStroke(this SKPaint paint, OxyColor stroke, double thickness, double scale, double[] dashArray = null, LineJoin lineJoin = LineJoin.Miter, bool aliased = false)
        {
            paint.Style = SKPaintStyle.Stroke;
            paint.Color = stroke.ToSKColor();
            paint.StrokeWidth = Convert(thickness, scale);
            paint.StrokeJoin = lineJoin.SKConvert();
            if (dashArray != null)
            {
                var dashArrayF = dashArray.Select(d => Convert(d, scale)).ToArray();
                paint.PathEffect = SKPathEffect.CreateDash(dashArrayF, 0f);
            }
        }

        /// <summary>
		/// Converts the specified coordinate to a scaled coordinate.
		/// </summary>
		/// <param name="x">The coordinate to convert.</param>
		/// <returns>The converted coordinate.</returns>
		private static float Convert(double x, double scale)
        {
            return (float)(x * scale);
        }
    }
}
