﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace OxyPlot.Xamarin.Shared
{
    public class PaintsStorage
    {
        private SKPaint[] _paints;

        public PaintsStorage()
        {
            var values = Enum.GetValues(typeof(PaintKind));
            _paints = new SKPaint[values.Length];
        }

        public SKPaint GetPaint(PaintKind kind)
        {
            int paintIndex = (int)kind;
            SKPaint paint = _paints[paintIndex];
            if (paint == null)
            {
                paint = new SKPaint();
                paint.Reset();
                _paints[paintIndex] = paint;
            }

            return paint;
        }
    }

    public enum PaintKind
    {
        Fill,
        Stroke,
        Line,
        Text,
        Image,
    }
}
